package ru.kweb.seo.excel;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.xpath.operations.Mod;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.model.report.ModelAnalyserTop;
import ru.kweb.seo.top.model.report.TagModel;
import scala.concurrent.impl.CallbackRunnable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
/**
 * Класс служит для создания excel отчета

 * @version 1.0
 */
public class CreatorExcel {

    private String path;

    public CreatorExcel() {
    }

    /**
     * Функция возвращает true как только сделан анализ по последнему запросу
     * @param modelAnalyserTops Список анализов по всем запросам
     * @param region название региона для отчета
     * @param path путь где будет лежать файл при создании excel файл
     * */
    public synchronized boolean create(List<ModelAnalyserTop> modelAnalyserTops, String region, String path){

        this.path = path;
        // создание самого excel файла в памяти
        HSSFWorkbook workbook = new HSSFWorkbook();
        // создание листа с названием "Просто лист"
        HSSFSheet sheet = workbook.createSheet("Анализ");

        // счетчик для строк
        int rowNum = 0;

        // Подпись к региону (это будет первая строчка в листе Excel файла)
        Row row = sheet.createRow(rowNum);
        row.createCell(0).setCellValue("Регион продвижения: ");
        row.createCell(1).setCellValue(region);
        // создаем подписи к столбцам
        row = sheet.createRow(++rowNum);
        row.createCell(0).setCellValue("Ключевая фраза");
        row.createCell(1).setCellValue("Частотность фразы");
        row.createCell(2).setCellValue("Рекомендованное количество символов");
        row.createCell(3).setCellValue("Рекомендованный % содержания ключевого запроса");
        row.createCell(4).setCellValue("Количество заголовков h1");
        row.createCell(5).setCellValue("Количество заголовков h2");
        row.createCell(6).setCellValue("Количество заголовков h3");
        row.createCell(7).setCellValue("Количество заголовков h4");

        // заполняем лист данными
        for(ModelAnalyserTop top : modelAnalyserTops) {
            createSheetHeader(sheet, ++rowNum, top);
        }

        // записываем созданный в памяти Excel документ в файл
        write(workbook);

        return true;
    }

    /**
     * Функция которая заполянет 1 строку в excel таблицы
     * @param sheet общий лист где храниться информация
     * @param rowNum номер строчки куда буедт сохраняться
     * @param modelAnalyserTops путь где будет лежать файл при создании excel файл
     * */
    private void createSheetHeader(HSSFSheet sheet, int rowNum, ModelAnalyserTop modelAnalyserTops) {
        Row row = sheet.createRow(rowNum);
        row.createCell(0).setCellValue(modelAnalyserTops.getPhrases());
        row.createCell(1).setCellValue("-");
        double temp;
        TagModel model = null;
        for(TagModel tagModel : modelAnalyserTops.getTags()){
            if(tagModel.getNameTag().equals("p")){
                model = tagModel;
            }
        }
        temp = Math.round(model.getCountChar());
        row.createCell(2).setCellValue(temp);
        row.createCell(3).setCellValue(model.getPercentSearchPhrases());
        for(TagModel tagModel : modelAnalyserTops.getTags()){
            if(tagModel.getNameTag().equals("h1")){
                temp = Math.round(tagModel.getCount()/10);
                row.createCell(4).setCellValue(temp);
            }
            if(tagModel.getNameTag().equals("h2")){
                temp = Math.round(tagModel.getCount()/10);
                row.createCell(5).setCellValue(temp);
            }
            if(tagModel.getNameTag().equals("h3")){
                temp = Math.round(tagModel.getCount()/10);
                row.createCell(6).setCellValue(temp);

            }
            if(tagModel.getNameTag().equals("h4")){
                temp = Math.round(tagModel.getCount()/10);
                row.createCell(7).setCellValue(temp);
            }
        }

    }
    /**
     * Функция создает документ
     * @param workbook класс внешней библиотеке
     * */
    private void write(HSSFWorkbook workbook){
        try (FileOutputStream out = new FileOutputStream(new File(path))) {
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Excel файл успешно создан!");
    }

}
