package ru.kweb.seo.top.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AliasFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kweb.seo.top.configuration.View;
import ru.kweb.seo.top.json.JsonResponse;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.model.SiteTagAnalyses;
import ru.kweb.seo.top.model.json.JsonRequest;
import ru.kweb.seo.top.model.report.ModelAnalyserTop;

import java.util.Date;
import java.util.List;

/**
 * Класс контроллер. Предназначен для принятия всех запросов на сервис и распределение их по функциям

 * @version 1.0
 */
@Controller
public class AnalyseController {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(AnalyseController.class.getName());

    /** bнициализация класса DistributorRequestService */
    @Autowired
    DistributorRequestService distributorRequestService;

    /**
     *  Функция отправляет запрос в обработчик запроса {@link DistributorRequestService#existsTop(JsonRequest)}
     *  Подает заявку на анализ топа сайта, в случаи отсутсыие его в бд ---/existsTop]
     *  @param searchPhrase поисковая фраза для яндекса
     *  @param searchRegion поисковой регион для яндекса(с front-end приходит валидный регион цифрой)
     *  @return возвращает json ответ {@link JsonResponse#getYes()} или {@link JsonResponse#getNo()}
     * */
    @CrossOrigin(origins = {"http://seo.poseosh.im","http://poseosh.im.work.ru"})
    @RequestMapping(value = "/existsTop", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<String> existsTop(@RequestParam("searchPhrase") String searchPhrase,
                                            @RequestParam("searchRegion") int searchRegion) {
        Date nowDate = new Date();
        JsonRequest request = new JsonRequest();
        request.setRegionNumber(searchRegion);
        request.setSearchPhrases(searchPhrase);
        request.setDate(nowDate.getTime());
        return new ResponseEntity<String>(distributorRequestService.existsTop(request), HttpStatus.OK);
    }

    /**
     *  Функция отправляет запрос в обработчик запроса {@link DistributorRequestService#getAnalysesSite(JsonRequest)}
     *  @param searchPhrase поисковая фраза для яндекса
     *  @param searchRegion поисковой регион для яндекса(с front-end приходит валидный регион цифрой)
     *  @return возвращает json ответ {@link JsonResponse#getReport(ModelAnalyserTop)}  или {@link JsonResponse#exceptionJson()}
     * */
    @CrossOrigin(origins = {"http://seo.poseosh.im","http://poseosh.im.work.ru"})
    @RequestMapping(value = "/getTopAnalyses", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<String> getTopAnalyses(@RequestParam("searchPhrase") String searchPhrase,
                                                 @RequestParam("searchRegion") int searchRegion) {
        JsonRequest request = new JsonRequest();
        request.setRegionNumber(searchRegion);
        request.setSearchPhrases(searchPhrase);
        log.debug("Request: /getTopAnalyses: " + request.toString());
        return new ResponseEntity<String>(distributorRequestService.getTopAnalyses(request), HttpStatus.OK);
    }

    /**
     *  Функция отправляет запрос в обработчик запроса {@link DistributorRequestService#getAnalysesSite(JsonRequest)}
     *  @param searchPhrase поисковая фраза
     *  @param pageAddress адресс сайта пользователя для анализа
     *  @return возвращает json ответ {@link JsonResponse#getAnalysesSite(SiteModel)}  или {@link JsonResponse#exceptionJson()}
     * */
    @CrossOrigin(origins = {"http://seo.poseosh.im","http://poseosh.im.work.ru"})
    @JsonView(View.SiteModel.class)
    @RequestMapping(value = "/getAnalysesSite", method = RequestMethod.GET, produces = {"application/json"})
    public @ResponseBody
    ResponseEntity<String> getAnalysesSite(@RequestParam("pageAddress") String pageAddress,
                                           @RequestParam("searchPhrase") String searchPhrase) {
        JsonRequest request = new JsonRequest();
        request.setNameSite(pageAddress);
        request.setSearchPhrases(searchPhrase);
        log.debug("Request: /getAnalysesSite: " + request.toString());
        return new ResponseEntity<String>(distributorRequestService.getAnalysesSite(request), HttpStatus.OK);
    }

    /**
     *  Функция отправляет запрос в обработчик запроса {@link DistributorRequestService#getAnalysesTag(String, String)}
     *  @param tagText текс 1 тэга
     *  @param searchPhrase поисковая фраза
     *  @return возвращает json ответ {@link JsonResponse#getTagAnalyses(SiteTagAnalyses)} или {@link JsonResponse#exceptionJson()}
     * */
    @CrossOrigin(origins = {"http://seo.poseosh.im","http://poseosh.im.work.ru"})
    @RequestMapping(value = "/getAnalysesTag", method = RequestMethod.GET, produces = {"application/json"})
    public @ResponseBody
    ResponseEntity<String> getAnalysesTag(@RequestParam("tagText") String tagText,
                                          @RequestParam("searchPhrase") String searchPhrase) {
        log.debug("Request: /getAnalysesTag.");
        return new ResponseEntity<String>(distributorRequestService.getAnalysesTag(tagText, searchPhrase), HttpStatus.OK);
    }

    /**
     *  Функция отправляет запрос в обработчик запроса {@link DistributorRequestService#getAutoSelectionWordWithPage(JsonRequest)}
     *  1) Находит топ 100 страниц которые больше всего подходят для продвижения
     *  2) К каждой их страницы подбирает лучшие 3 слова(совет в выборе фразы под страницу)
     *  @param url сайт пользователя
     *  @return возвращает json ответ {@link JsonResponse#getModelAutoSelection(List)} или {@link JsonResponse#exceptionJson()}
     * */
    @CrossOrigin(origins = {"http://seo.poseosh.im","http://poseosh.im.work.ru"})
    @RequestMapping(value = "/getAutoSelectionWordWithPage", method = RequestMethod.GET, produces = {"application/json"})
    public @ResponseBody
    ResponseEntity<String> getAutoSelectionWordWithPage(@RequestParam("url") String url) {
        JsonRequest request = new JsonRequest();
        /** Валидация для url {@link #validationSlash(String)}*/
        request.setNameSite(validationSlash(url));
        log.debug("Request: /getAutoSelectionWordWithPage.");
        return new ResponseEntity<String>(distributorRequestService.getAutoSelectionWordWithPage(request), HttpStatus.OK);
    }

    /**
     *  Функция отправляет запрос в обработчик запроса {@link DistributorRequestService#getPosition(JsonRequest, String)}
     *  @param id уникальный id пересылаеться front-end нужен для аинзроных запросов(Возможно костыль фронта)
     *  @param url сайт пользователя
     *  @param searchPhrase поисковая фраза
     *  @param searchRegion поисковой регион(с front-end приходит валидный регион цифрой)
     *  @return возвращает json ответ {@link JsonResponse#getPosition(String, String)} или {@link JsonResponse#exceptionJson()}
     * */
    @CrossOrigin(origins = {"http://seo.poseosh.im","http://poseosh.im.work.ru"})
    @RequestMapping(value = "/getPosition", method = RequestMethod.GET, produces = {"application/json"})
    public @ResponseBody
    ResponseEntity<String> getPosition(@RequestParam("id") String id,
                                       @RequestParam("url") String url,
                                       @RequestParam("searchPhrase") String searchPhrase,
                                       @RequestParam("searchRegion") int searchRegion) {
        JsonRequest request = new JsonRequest();
        request.setNameSite(url);
        request.setSearchPhrases(searchPhrase);
        request.setRegionNumber(searchRegion);
        log.debug("Request: /getPosition.");
        return new ResponseEntity<String>(distributorRequestService.getPosition(request, id), HttpStatus.OK);
    }
    /**
     *  Функция отправляет запрос в обработчик запроса {@link DistributorRequestService#getTitle(JsonRequest, String)}
     *  @param id уникальный id пересылаеться front-end нужен для аинзроных запросов(Возможно костыль фронта)
     *  @param url сайт пользователя
     *  @return возвращает json ответ {@link JsonResponse#getTitle(String, String)}  или {@link JsonResponse#exceptionJson()}
     * */
    @CrossOrigin(origins = {"http://seo.poseosh.im","http://poseosh.im.work.ru"})
    @AliasFor(value = "http://seo.poseosh.im")
    @RequestMapping(value = "/getTitle", method = RequestMethod.GET, produces = {"application/json"})
    public @ResponseBody
    ResponseEntity<String> getTitle(@RequestParam("id") String id,
                                    @RequestParam("url") String url) {
        JsonRequest request = new JsonRequest();
        request.setNameSite(url);
        log.debug("Request: /getTitle.");
        return new ResponseEntity<String>(distributorRequestService.getTitle(request, id), HttpStatus.OK);
    }

    /**
     *  Функция отправляет запрос в обработчик запроса {@link DistributorRequestService#report(String, String, String, String)}
     *  @param searchRegion поисковой регион (Должен быть числом!)
     *  @param region сайт пользователя (Расшифровка региона)
     *  @param mail почтапользовтеля
     *  @param body json со списком страниц
     *  @return возвращает json ответ {@link HttpStatus#OK}
     * */
    @CrossOrigin(origins = {"http://seo.poseosh.im","http://poseosh.im.work.ru"})
    @RequestMapping(value = "/report", method = RequestMethod.POST, produces = {"application/json"})
    public @ResponseBody
    ResponseEntity<String> report(@RequestBody() String body,
                                  @RequestParam("searchRegion") String searchRegion,
                                  @RequestParam("region") String region,
                                  @RequestParam("mail") String mail) {
        log.debug("Request: /report");
        distributorRequestService.report(body, searchRegion, region, mail);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    /**
     *  Функция администратора, предоставляет первоначальный анализ сайтов сразу на почту отправителя
     *  @param url поисковой регион (Должен быть числом!)
     *  @return ответ без "/" , если он был.
     * */
    private String validationSlash(String url){
        try {
            if (url.charAt(url.length()-1)=="/".charAt(0))
                return url.substring(0,url.length()-1);
        }catch (StringIndexOutOfBoundsException e){
        }
        return url;
    }
}
