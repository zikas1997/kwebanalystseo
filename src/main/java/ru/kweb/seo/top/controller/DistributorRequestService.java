package ru.kweb.seo.top.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kweb.seo.excel.CreatorExcel;
import ru.kweb.seo.top.analyses.AnalyseFullTagListSite;
import ru.kweb.seo.top.analyses.AnalyserOneTag;
import ru.kweb.seo.top.creator.*;
import ru.kweb.seo.top.json.JsonBodyRequest;
import ru.kweb.seo.top.json.JsonResponse;
import ru.kweb.seo.top.mail.EmailSender;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.model.SiteTagAnalyses;
import ru.kweb.seo.top.model.analyses.ModelAutoSelectionPageAndWord;
import ru.kweb.seo.top.model.json.JsonRequest;
import ru.kweb.seo.top.model.position.SitePosition;
import ru.kweb.seo.top.model.report.ModelAnalyserTop;
import ru.kweb.seo.top.repository.ListSiteRepositoryServiceImplement;
import ru.kweb.seo.top.repository.PositionSiteRepository;

import java.util.Date;
import java.util.List;

/**
 * Класс сервис, перенаправляющий результат запросов по сревисам. Центральный класс знающий почти о всех классах сервисов.

 * @version 1.0
 */
@Service
public class DistributorRequestService {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(DistributorRequestService.class.getName());

    /** инициализация репозитория List Site */
    @Autowired
    private ListSiteRepositoryServiceImplement repositoryImplement;

    /** инициализация сервиса по работе с очередью заявок */
    @Autowired
    private CreatorAnalysesTopSiteService siteService;

    /** инициализация сервиса по анализу сайта */
    @Autowired
    private CreatorAnalysesSiteService creatorAnalysesSiteService;

    /** инициализация репозитория Position Site */
    @Autowired
    private PositionSiteRepository positionSiteRepository;

    /** Класс отвчеающий за создание анализа */
    @Autowired
    CreatorReportAnalysesService creatorReportAnalysesService;

    /** Класс подготовки ответов в Json */
    private JsonResponse jsonResponse = new JsonResponse();

    /** Класс расшифроки bode Json */
    private JsonBodyRequest jsonRequest = new JsonBodyRequest();

    /** Класс отправки писем EmailSender*/
    @Autowired
    EmailSender emailSender;

    /** Создает новый объект сервиса DistributorRequestService */
    public DistributorRequestService() {
    }

    /**
     *  Функция проверяет, есть ли анализ в базе, в случаи отсутсвия добавляет в очередь запрос на исполнение
     *  Подает заявку на анализ топа сайта, в случаи отсутсыие его в бд ---/existsTop]
     *  @param request классичекий запрос с сервера
     *  @return возвращает json ответ {@link JsonResponse#getYes()} или {@link JsonResponse#getNo()}
     * */
    public String existsTop(JsonRequest request) {
        if (repositoryImplement.existTop(request.getSearchPhrases(), request.getRegionNumber())) {
            return jsonResponse.getYes();
        }
        siteService.addRequest(request);
        return jsonResponse.getNo();
    }

    /**
     *  Функция возвращает результат по анализу
     *  @param request классичекий запрос с сервера
     *  @return возвращает json ответ {@link JsonResponse#getReport(ModelAnalyserTop)}  или {@link JsonResponse#exceptionJson()}
     * */
    public String getTopAnalyses(JsonRequest request) {
        ModelAnalyserTop top = creatorReportAnalysesService.create(request.getSearchPhrases(),request.getRegionNumber());
        if (top == null) {
            return jsonResponse.exceptionJson();
        }
        return jsonResponse.getReport(top);
    }

    /**
     *  Функция возвращает результат по анализу сайта пользователя
     *  @param request классичекий запрос с сервера
     *  @return возвращает json ответ {@link JsonResponse#getAnalysesSite(SiteModel)}  или {@link JsonResponse#exceptionJson()}
     * */
    public String getAnalysesSite(JsonRequest request) {
        SiteModel siteModel = creatorAnalysesSiteService.getAnalysesSite(request);
        if (siteModel == null) {
            return jsonResponse.exceptionJson();
        }
        AnalyseFullTagListSite analyseFullTagListSite  = new AnalyseFullTagListSite();
        analyseFullTagListSite.analyse(siteModel,request.getSearchPhrases(),String.valueOf(request.getRegionNumber()));
        siteModel.setModelAnalyserTop(analyseFullTagListSite.getModelAnalyserTop());
        return jsonResponse.getAnalysesSite(siteModel);
    }

    /**
     *  Функция возвращает результат по анализу сайта пользователя
     *  @param tagText текс 1 тэга
     *  @param searchPhrase поисковая фраза
     *  @return возвращает json ответ {@link JsonResponse#getTagAnalyses(SiteTagAnalyses)} или {@link JsonResponse#exceptionJson()}
     * */
    public String getAnalysesTag(String tagText, String searchPhrase) {
        SiteTagAnalyses analyses = new SiteTagAnalyses();
        AnalyserOneTag analyserOneTag = new AnalyserOneTag(tagText, searchPhrase);
        try {
            analyses.setTegName(tagText);
            analyses.setCountChar(analyserOneTag.getCountChar());
            analyses.setCountInputPhrase(analyserOneTag.getCountDirectOccurrencesPhrase());
            analyses.setCountWord(analyserOneTag.getCountWord());
            analyses.setPositionFirstPhrasesWord(analyserOneTag.getFirstPositionOccurrences());
            return jsonResponse.getTagAnalyses(analyses);
        } catch (Exception e) {
            log.error("Exception create one tag analyses: ", e);
            return jsonResponse.exceptionJson();
        }
    }

    /**
     *  Функция по адресу сайта:
     *  1) Находит топ 100 страниц которые больше всего подходят для продвижения
     *  2) К каждой их страницы подбирает лучшие 3 слова(совет в выборе фразы под страницу)
     *  @param request классичекий запрос с сервера
     *  @return возвращает json ответ {@link JsonResponse#getModelAutoSelection(List)} или {@link JsonResponse#exceptionJson()}
     * */
    public String getAutoSelectionWordWithPage(JsonRequest request) {
        try {
            //Получаем список страниц по стайту
            CreatorDomainPages creatorDomainPages = new CreatorDomainPages();
            List<String> listDomain = creatorDomainPages.getListPagesDomain(request);
            //Подбираем под страницы ключивые слова
            CreatorListBestWords creatorListBestWords = new CreatorListBestWords();
            List<ModelAutoSelectionPageAndWord> stringList = creatorListBestWords.create(listDomain);
            return jsonResponse.getModelAutoSelection(stringList);
        } catch (Exception e) {
            log.error("Exception create one tag analyses: ", e);
            return jsonResponse.exceptionJson();
        }
    }

    /**
     *  Функция выдает позицию сайта и кэшируют ее на 2 недели
     *  @param id уникальный id пересылаеться front-end нужен для аинзроных запросов(Возможно костыль фронта)
     *  @param request классичекий запрос с сервера
     *  @return возвращает json ответ {@link JsonResponse#getPosition(String, String)} или {@link JsonResponse#exceptionJson()}
     * */
    public String getPosition(JsonRequest request,String id) {

        String url = request.getNameSite();
        String searchPhrases = request.getSearchPhrases();
        String regionNumber = String.valueOf(request.getRegionNumber());

        //Проверяем есть ли в базе ячейка под позицию
        if (positionSiteRepository.existsByUrlAndSearchPhrasesAndSearchRegion(url,searchPhrases,regionNumber)) {

            SitePosition sitePosition = positionSiteRepository.findByUrlAndSearchPhrasesAndSearchRegion(url,searchPhrases,regionNumber);

            //Если позиция не равана null то выйдаем результат позиции
            while (sitePosition.getPositionSite().equals("null")){
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sitePosition = positionSiteRepository.findByUrlAndSearchPhrasesAndSearchRegion(url,searchPhrases,regionNumber);
            }
            return jsonResponse.getPosition(sitePosition.getPositionSite(),id);

        }

        //Создаем пустой объект
        SitePosition sitePosition = new SitePosition();
        sitePosition.setUrl(url);
        sitePosition.setSearchPhrases(searchPhrases);
        sitePosition.setSearchRegion(regionNumber);
        sitePosition.setPositionSite("null");
        positionSiteRepository.save(sitePosition);

        try {
            //Проводим анализ
            CreatorPositionPage creatorDomainPages = new CreatorPositionPage();
            String position = creatorDomainPages.getPosition(request);
            sitePosition.setPositionSite(position);
            sitePosition.setLastTime(new Date().getTime());
            //Сохраняем анализ
            positionSiteRepository.save(sitePosition);
            return jsonResponse.getPosition(position,id);
        } catch (Exception e) {
            log.error("Exception create one tag analyses: ", e);
            return jsonResponse.exceptionJson();
        }
    }

    /**
     *  Функция получает-title- с сайта
     *  @param id уникальный id пересылаеться front-end нужен для аинзроных запросов(Возможно костыль фронта)
     *  @param request классичекий запрос с сервера
     *  @return возвращает json ответ {@link JsonResponse#getTitle(String, String)}  или {@link JsonResponse#exceptionJson()}
     * */
    public String getTitle(JsonRequest request, String id) {
        try {
            CreatorAnalysesSiteService creator = new CreatorAnalysesSiteService();
            SiteModel sm = creator.getAnalysesSite(request);
            return jsonResponse.getTitle(sm.getTitle().getTextTitle(),id);
        } catch (Exception e) {
            log.error("Exception create one tag analyses: ", e);
            return jsonResponse.exceptionJson();
        }
    }

    /**
     *  Процедура администратора, предоставляет первоначальный анализ сайтов сразу на почту отправителя
     *  @param searchRegion поисковой регион (Должен быть числом!)
     *  @param region сайт пользователя (Расшифровка региона)
     *  @param mail почтапользовтеля
     *  @param json json со списком страниц
     * */
    public void report(String json, String searchRegion, String region, String mail) {

        String[] allSearchPhrases = jsonRequest.allSearchPhrases(json);
        try {
            CreatorExcel creatorExcel = new CreatorExcel();
            List<ModelAnalyserTop> top = creatorReportAnalysesService.create(allSearchPhrases,Integer.parseInt(searchRegion));
            if(creatorExcel.create(top,region,"D:/java/poseosh.xls")){
                emailSender.sendMessageWithAttachment(mail,"Seo Poseosh отчет","Отчет","D:/java/poseosh.xls");
                log.debug("Письмо отправлено! Адресс: " + mail + "Регион: " + region);
            }else {
                log.error("Оишбка отправки письма! Адресс: " + mail + "Регион: " + region);
            }
        } catch (Exception e) {
            log.error("Exception create one tag analyses: ", e);
        }
    }
}
