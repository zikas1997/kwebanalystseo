package ru.kweb.seo.top.ethernet;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
/**
 * Класс использует библиотеку HttpApache отправляет запрос и получает результат

 * @version 1.0
 */
public class HttpApacheClient implements HttpRequest {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(HttpApacheClient.class.getName());

    private HttpClient client = HttpClientBuilder.create().build();

    public HttpApacheClient() {
    }

    /**
     * Функция отправляет запрос и получает результат по запросу
     * @param url адресс сайта
     * @return возвращает html странциы {@link String}, если статус !=200 возвращает null
     * */
    @Override
    public String getResponse(String url) {
        HttpGet request = new HttpGet(url);
        HttpResponse response = null;

        StringBuffer result = new StringBuffer();
        try {
            response = client.execute(request);
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (MalformedURLException e) {
            log.error("Time Malformed URL exception : " + request);
        } catch (SocketTimeoutException e) {
            log.error("Time out exception url: " + request);
        } catch (IOException e) {
            log.error("URL exception: " + request, e);
        }

        return result.toString();
    }

}
