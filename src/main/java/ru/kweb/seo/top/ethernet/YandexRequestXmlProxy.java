package ru.kweb.seo.top.ethernet;

import ru.kweb.seo.top.model.json.JsonRequest;

/**
 * Класс занимаеться подготовкой запроса в http://xmlproxy.ru

 * @version 1.0
 */
public final class YandexRequestXmlProxy {

    public YandexRequestXmlProxy() {
    }

    /**
     * Функция отправляет запрос на получение топ 10 по запросу
     * @param request адресс сайта
     * @return возвращает xml ответ {@link String}
     * */
    public static String getResponse(JsonRequest request){
        HttpRequest httpGetterHtmlPage = new HttpApacheClient();
        String url = "http://xmlproxy.ru/search/xml?" +
                "query=" +request.getSearchPhrases().replaceAll(" ","%20") +
                "&lr="+request.getRegionNumber() +
                "&groupby=attr%3D%22%22.mode%3Dflat.groups-on-page%3D10.docs-in-group%3D1" +
                "&page=0" +
                "&user=info%40kweb.studio&key=MTUyMjY3MTE1NzEzMTAxNTE4MTM2ODgxNjQz";
        return httpGetterHtmlPage.getResponse(url);
    }

    /**
     * Функция отправляет запрос на получение 100 релевантных странци
     * @param request адресс сайта
     * @return возвращает xml ответ {@link String}
     * */
    public static String get100DomainByUrl(JsonRequest request){
        HttpRequest httpGetterHtmlPage = new HttpApacheClient();
        String url = "http://xmlproxy.ru/search/xml?" +
                "query=url:" +request.getNameSite().replaceAll(" ","%20") +
                "/*&groupby=attr%3D%22%22.mode%3Dflat.groups-on-page%3D100.docs-in-group%3D1" +
                "&page=0" +
                "&user=info%40kweb.studio&key=MTUyMjY3MTE1NzEzMTAxNTE4MTM2ODgxNjQz";

        return httpGetterHtmlPage.getResponse(url);
    }

    /**
     * Функция отправляет запрос на получение топ 100 по запросу
     * @param request адресс сайта
     * @return возвращает xml ответ {@link String}
     * */
    public static String get100Domain1BySP(JsonRequest request){
        HttpRequest httpGetterHtmlPage = new HttpApacheClient();
        String url = "http://xmlproxy.ru/search/xml?" +
                "query=" +request.getSearchPhrases().replaceAll(" ","%20") +
                "&lr="+request.getRegionNumber() +
                "/*&groupby=attr%3D%22%22.mode%3Dflat.groups-on-page%3D100.docs-in-group%3D1" +
                "&page=0" +
                "&user=info%40kweb.studio&key=MTUyMjY3MTE1NzEzMTAxNTE4MTM2ODgxNjQz";
        return httpGetterHtmlPage.getResponse(url);
    }

}
