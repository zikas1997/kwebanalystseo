package ru.kweb.seo.top.ethernet;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kweb.seo.top.model.analyses.SiteHtmlModel;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
/**
 * Класс использует библиотеку HtmlUnit отправляет запрос и получает результат

 * @version 1.0
 */
public class HtmlUnitClient implements HttpRequest {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(HtmlUnitClient.class.getName());

    private String result;

    public HtmlUnitClient() {
    }

    /**
     * Функция отправляет запрос и в случаи, статус ответ 200 возвращяет html старницу
     * @param url адресс сайта
     * @return возвращает html странциы {@link String}, если статус !=200 возвращает null
     * */
    @Override
    public String getResponse(String url) {
        WebClient webClient = getWebClient();
        Page page = null;
        WebResponse response = null;
        try {
            if (webClient.getPage(url).getWebResponse().getStatusCode() == 200) {

                page = webClient.getPage(url);
                response = page.getWebResponse();
                result = response.getContentAsString();
            } else {
                log.error("Response status code don't 200: " + url + " Status: " + webClient.getPage(url).getWebResponse().getStatusCode());
            }
        }catch (MalformedURLException e){
            log.error("Malformed URL Exception: " + url);
        }catch (SocketTimeoutException e) {
            log.error("Time out exception url: " + url);
        } catch (UnknownHostException e1) {
            log.error("URL Unknown Host Exception: " + url, e1);
        } catch (IOException e) {
            log.error("URL exception: " + url, e);
        }
        webClient.close();
        return result;
    }

    /**
     * Функция отправляет запрос и в случаи, статус ответ 200 возвращяет расширеный объект с информацией
     * @param url адресс сайта
     * @return возвращает расширеный объект странциы {@link SiteHtmlModel}, если статус !=200 возвращает null
     * */
    public SiteHtmlModel getSite(String url) {
        WebClient webClient = getWebClient();
        SiteHtmlModel siteHtmlModel = null;
        Page page = null;
        WebResponse response = null;
        try {
            if (webClient.getPage(url).getWebResponse().getStatusCode() == 200) {
                page = webClient.getPage(url);
                response = page.getWebResponse();
                siteHtmlModel = new SiteHtmlModel();
                siteHtmlModel.setHtmlPage(response.getContentAsString());
                siteHtmlModel.setNamePage(url);
            } else {
                log.error("Response status code don't 200: " + url + " Status: " + webClient.getPage(url).getWebResponse().getStatusCode());
            }
        }catch (MalformedURLException e){
            log.error("Malformed URL Exception: " + url);
        } catch (SocketTimeoutException e) {
            log.error("Time out exception url: " + url);
        } catch (UnknownHostException e1) {
            log.error("URL exception: " + url, e1);
        } catch (IOException e) {
            log.error("URL exception: " + url, e);
        }

        webClient.close();
        return siteHtmlModel;
    }

    /**
     * Функция отправляет запрос и в случаи, статус ответ 200 время отклика сервира
     * @param url адресс сайта
     * @return возвращает время отклика {@link Long}
     * */
    public long getLoadTime(String url) {
        WebClient webClient = getWebClient();

        try {
            if (webClient.getPage(url).getWebResponse().getStatusCode() == 200) {
                return webClient.getPage(url).getWebResponse().getLoadTime();
            } else {
                log.error("Response status code don't 200: " + url + " Status: " + webClient.getPage(url).getWebResponse().getStatusCode());

            }
        }catch (MalformedURLException e){
            log.error("Malformed URL Exception: " + url);
        } catch (SocketTimeoutException e) {
            log.error("Time out exception url: " + url);
        } catch (UnknownHostException e1) {
            log.error("URL exception: " + url, e1);
        } catch (IOException e) {
            log.error("URL exception: " + url, e);
        }
        webClient.close();

        return -1;
    }

    /**
     * Функция создает WebClient и дает ему стандартные настройки
     * @return возвращает объект {@link WebClient}
     * */
    private WebClient getWebClient() {
        WebClient webClient = new WebClient();
        webClient.getOptions().setJavaScriptEnabled(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setUseInsecureSSL(true);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setDownloadImages(false);
        webClient.getOptions().setGeolocationEnabled(false);
        webClient.getOptions().setRedirectEnabled(false);
        webClient.getOptions().setPrintContentOnFailingStatusCode(false);
        webClient.getOptions().setTimeout(5500);
        return webClient;
    }
}
