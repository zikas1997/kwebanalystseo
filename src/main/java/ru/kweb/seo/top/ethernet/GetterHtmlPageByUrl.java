package ru.kweb.seo.top.ethernet;


import ru.kweb.seo.top.counter.Counter;
import ru.kweb.seo.top.model.analyses.SiteHtmlModel;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
/**
 * Класс являеться статичным и служит для получение html страницы по запросу

 * @version 1.0
 */
public final class GetterHtmlPageByUrl {

    private GetterHtmlPageByUrl() {
    }

    /**
     * Функция в МНОГОПОТОЧНОМ режими отправляет запросы на исполнения и ждет результат
     * 1. Если запрос проходит html страница не null - Запись в список
     * 2. Если запрос проходит html страница равна null - Запись не происходит
     * @param sitesAddress список адресов сайтов для подбора
     * @return возвращает html странциы по каждому запросу
     * */
    public static List<String> getTopHtml(List<String> sitesAddress) {
        boolean flag = true;
        HttpRequest request = new HtmlUnitClient();
        CopyOnWriteArrayList<String> htmlPageList = new CopyOnWriteArrayList<>();
        Counter k = new Counter(sitesAddress.size());
        for (String site : sitesAddress) {
            Thread myThready = new Thread(new Runnable() {
                public void run() {
                    String s = request.getResponse(site);
                    if (s != null)
                        htmlPageList.add(s);
                    k.countMinus();
                }
            });
            myThready.start();    //Запуск потока
        }

        while (flag) {
            if (k.getK() == 0) {
                flag = false;
            }
        }

        return htmlPageList;
    }

    /**
     * Функция возвращает html страницу
     * @param url адресс сайта
     * @return возвращает html странциу {@link String}
     * */
    public static String getHtmlPage(String url) {
        HttpRequest request = new HtmlUnitClient();
        return request.getResponse(url);
    }

    /**
     * Функция в МНОГОПОТОЧНОМ режими отправляет запросы на исполнения и ждет результат
     * 1. Если запрос проходит html страница не null - Запись в список
     * 2. Если запрос проходит html страница равна null - Запись не происходит
     * !!! отличние от {@link #getTopHtml(List)} в том, что возвращает полную модель адресс-страница !!!
     * @param sitesAddress список адресов сайтов для подбора
     * @return возвращает html странциы по каждому запросу
     * */
    public static List<SiteHtmlModel> getHtmlPagesWithNamePage(List<String> sitesAddress) {
        boolean flag = true;
        HttpRequest request = new HtmlUnitClient();
        CopyOnWriteArrayList<SiteHtmlModel> htmlPageList = new CopyOnWriteArrayList<>();
        Counter k = new Counter(sitesAddress.size());
        for (String site : sitesAddress) {
            Thread myThready = new Thread(new Runnable() {
                public void run() {
                    SiteHtmlModel s = ((HtmlUnitClient) request).getSite(site);
                    if (s != null)
                        htmlPageList.add(s);
                    k.countMinus();
                }
            });
            myThready.start();    //Запуск потока
        }

        while (flag) {
            if (k.getK() == 0) {
                flag = false;
            }
        }

        return htmlPageList;
    }

    /**
     * Функция возвращает время отклика страницы
     * @param url адресс сайта
     * @return время отклика @link Long}
     * */
    public static long getTime(String url) {
        HttpRequest request = new HtmlUnitClient();
        return ((HtmlUnitClient) request).getLoadTime(url);
    }

}
