package ru.kweb.seo.top.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.stachek66.nlp.mystem.holding.Factory;
import ru.stachek66.nlp.mystem.holding.MyStem;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;
import ru.stachek66.nlp.mystem.holding.Request;
import ru.stachek66.nlp.mystem.model.Info;
import scala.Option;
import scala.collection.JavaConversions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class StemAnalyzer {

    /*---Logger---*/
    private final static Logger log = LoggerFactory.getLogger(StemAnalyzer.class.getName());

    private final static MyStem mystemAnalyzer =
            new Factory(" -igd --eng-gr --format json ")
                    .newMyStem("3.0", Option.<File>empty()).get();

    public static List<String> GetLex(String str, boolean englishWord) throws MyStemApplicationException {

        List<String> lex = new ArrayList<>();
        Iterable<Info> result =
                JavaConversions.asJavaIterable(
                        mystemAnalyzer
                                    .analyze(Request.apply(str))
                                .info()
                                .toIterable());

        for (Info info : result) {
            if (isGoodWord(info,englishWord))
                lex.add(
                        serializeLex(
                                info.rawResponse()));
        }

        return lex;
    }

    private static String serializeLex(String json) {
        String text;
        JsonObject object = new JsonParser().parse(json).getAsJsonObject();
        JsonArray jsonElements = (JsonArray) object.get("analysis");
        for (JsonElement jsonElement : jsonElements) {
            text = jsonElement.getAsJsonObject().get("lex").getAsString();
            return text;
        }
        text = serializeText(json);
        return text;
    }

    private static String serializeGr(String json) {
        JsonObject object = new JsonParser().parse(json).getAsJsonObject();
        JsonArray jsonElements = (JsonArray) object.get("analysis");
        for (JsonElement jsonElement : jsonElements) {
            return jsonElement.getAsJsonObject().get("gr").getAsString();
        }
        return "";
    }

    private static String serializeText(String json) {
        return new JsonParser().parse(json).getAsJsonObject().get("text").getAsString();
    }

    /*
     * Параметр boolean указывает нужно ли учитывать при подсчете английские слова
     * */
    private static boolean isGoodWord(Info info, boolean englishWord) {
        char[] chars = serializeGr(info.rawResponse()).toCharArray();
        if (chars.length != 0) {
            if (chars[0] == "C".charAt(0)) {
                if (chars[3] == "J".charAt(0)) {
                    return false;
                }
            }
            if (chars[0] == "P".charAt(0)) {
                return false;
            }
            return true;
        } else {
            if (englishWord) {
                char ch = serializeText(info.rawResponse()).charAt(0);
                if ((ch >= 0x0041 && ch <= 0x005A) || (ch >= 0x0061 && ch <= 0x007A)) {
                    return true;
                }
            }
        }
        return false;
    }
}
