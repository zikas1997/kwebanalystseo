package ru.kweb.seo.top.counter;
/**
 * Класс счетчик

 * @version 1.0
 */
public class Counter {

    private volatile int k;

    public Counter(int k) {
        this.k = k;
    }

    public int getK() {
        return k;
    }

    public synchronized void countMinus() {
        k--;
    }

    public synchronized void countPlus() {
        k++;
    }
}
