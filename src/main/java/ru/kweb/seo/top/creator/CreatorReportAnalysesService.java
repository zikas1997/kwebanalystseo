package ru.kweb.seo.top.creator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kweb.seo.top.model.report.ModelAnalyserTop;
import ru.kweb.seo.top.analyses.AnalyseFullTagListSite;
import ru.kweb.seo.top.controller.DistributorRequestService;
import ru.kweb.seo.top.model.ListSite;
import ru.kweb.seo.top.model.json.JsonRequest;
import ru.kweb.seo.top.repository.ListSiteRepositoryServiceImplement;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс сервис выполняет роль отправки данных на анализ и сбора анализ в единый ответ

 * @version 1.0
 */
@Service
public class CreatorReportAnalysesService {

    /** Logger*/
    private final static Logger log = LoggerFactory.getLogger(CreatorReportAnalysesService.class.getName());

    /** инициализация репозитория List Site */
    @Autowired
    private ListSiteRepositoryServiceImplement repositoryImplement;

    /** инициализация сервиса по работе с очередью заявок*/
    @Autowired
    private DistributorRequestService requestService;

    private List<ListSite> allListSite;

    private List<ModelAnalyserTop> modelAnalyserTops;

    public CreatorReportAnalysesService() {
    }

    /**
     * Функция контролирует анализ от обработки запросов до выдачи ананлиза в готовом ввиде
     * !!!Для списка запросов!!!
     * @param searchPhrases поисковая фраза
     * @param region поисковой регион
     * @return возвращает готовый анализ по всем старнциам
     * */
    public List<ModelAnalyserTop> create(String[] searchPhrases, int region) {
        modelAnalyserTops = new ArrayList<>();
        allListSite = new ArrayList<>();

        //отправляем пачку запросов на исполнения
        for (String s : searchPhrases) {
            requestService.existsTop(new JsonRequest(s, region));
        }

        //Проверяем в бесконечном цикле, пока все запросы не запишутся в базу
        while (true) {
            for (int i = 0; i < searchPhrases.length; i++) {
                if (!repositoryImplement.existAnalyse(searchPhrases[i], region)) {
                    i = 0;
                }
            }
            break;
        }

        //Достаем сырой анализ из базы
        for (String s : searchPhrases) {
            allListSite.add(repositoryImplement.getTop(s, region));
        }

        for (ListSite listSite : allListSite) {
            modelAnalyserTops.add(
                    analyse(listSite));
        }

        return modelAnalyserTops;
    }

    /**
     * Функция контролирует анализ от обработки запросов до выдачи ананлиза в готовом ввиде
     * !!!Для топа по 1 запросу!!! (Топ)
     * @param searchPhrases поисковая фраза
     * @param region поисковой регион
     * @return возвращает готовый анализ по запросу {@link ModelAnalyserTop}
     * */
    public ModelAnalyserTop create(String searchPhrases, int region) {

        //отправляем пачку запросов на исполнения
        requestService.existsTop(new JsonRequest(searchPhrases, region));

        //Проверяем в бесконечном цикле, пока все запросы не запишутся в базу
        while (true) {
            if (repositoryImplement.existAnalyse(searchPhrases, region)) {
                break;
            }
        }

        //Достаем сырой анализ из базы
        ListSite listSite = repositoryImplement.getTop(searchPhrases, region);

       return analyse(listSite);
    }

    /**
     * Функция вызывает дополнительный анализ
     * @param listSite глобальная модель по 1 запросу
     * @return возвращает готовый анализ по всем старнциам {@link ModelAnalyserTop}
     * */
    private ModelAnalyserTop analyse(ListSite listSite) {

        AnalyseFullTagListSite ansTag = new AnalyseFullTagListSite();

        //Полный анализ и подготовка к отчету
        ansTag.analyse(listSite);
        ModelAnalyserTop top = ansTag.getModelAnalyserTop();

        return top;
    }
}
