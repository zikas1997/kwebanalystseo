package ru.kweb.seo.top.creator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.kweb.seo.top.analyses.AnalyserMetaTag;
import ru.kweb.seo.top.analyses.AnalysesAllTagsSiteModelService;
import ru.kweb.seo.top.analyses.AnalysesSEO;
import ru.kweb.seo.top.ethernet.GetterHtmlPageByUrl;
import ru.kweb.seo.top.model.json.JsonRequest;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.parsers.CreateSiteModelByHtmlPage;
import ru.kweb.seo.top.parsers.ParserSite;

/**
 * Класс сервис создающий {@link SiteModel}

 * @version 1.0
 */
@Service
public class CreatorAnalysesSiteService {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(CreatorAnalysesSiteService.class.getName());

    //-------------------------------------------
    //  Переменные для замерки выполнения методов
    //-------------------------------------------
    private long st;
    private long en;
    private double rs;
    //-------------------------------------------

    //-------------------------------------------
    //  Классы анализов
    //-------------------------------------------
    private AnalysesSEO seo = new AnalysesAllTagsSiteModelService();
    private AnalyserMetaTag analyserMetaTag = new AnalyserMetaTag();
    //-------------------------------------------

    /**
     * Функция делает и возвращает анализ по сайту
     * @param request запрос по сайту
     * @return возвращает готовывй анализ {@link SiteModel}
     * */
    public SiteModel getAnalysesSite(JsonRequest request) {

        SiteModel siteModel = createSiteModel(request);
        SiteModel resultSiteModel = null;
        if (siteModel != null)
            resultSiteModel = getAnalyses(siteModel, request.getSearchPhrases());
        return resultSiteModel;
    }

    /**
     * Функция делает основной разбор по тегам и пред анализ данных
     * @param request запрос по сайту
     * @return возвращает готовывй анализ {@link SiteModel}
     * */
    private SiteModel createSiteModel(JsonRequest request) {

        //-------------------------------------------------------------------------------------------
        // Получаем html page по url
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        String page = GetterHtmlPageByUrl.getHtmlPage(request.getNameSite());
        long time = GetterHtmlPageByUrl.getTime(request.getNameSite());
        en = System.nanoTime();
        rs = en - st;
        log.debug("Get html page by url request: " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------
        // Создаем SiteModel на основе html page
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        ParserSite createSiteModelByHtmlPage = new CreateSiteModelByHtmlPage();
        SiteModel resultSiteModel = null;
        if (page != null){
            resultSiteModel = createSiteModelByHtmlPage.getSiteModel(page, request.getNameSite());
            resultSiteModel.setLoadTime(time);
        }
        en = System.nanoTime();
        rs = en - st;
        log.debug("Create Site model by html: " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------

        return resultSiteModel;
    }

    /**
     * Функция делает окончательный анализ по тегам
     * @param sm подготовленая форма с тэгами
     * @param searchPhrases поисковая фраза
     * @return возвращает готовывй анализ {@link SiteModel}
     * */
    private SiteModel getAnalyses(SiteModel sm, String searchPhrases) {
        return seo.getAnalysesSiteModel(sm, searchPhrases);
    }
}
