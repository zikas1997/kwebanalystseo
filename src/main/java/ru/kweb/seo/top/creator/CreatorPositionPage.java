package ru.kweb.seo.top.creator;

import ru.kweb.seo.top.ethernet.YandexRequestXmlProxy;
import ru.kweb.seo.top.model.json.JsonRequest;
import ru.kweb.seo.top.parsers.ParserResponse;
import ru.kweb.seo.top.parsers.ParserXmlResponse;

import java.util.List;
/**
 * Класс определяет позицию сайта поисковик yandex

 * @version 1.0
 */
public class CreatorPositionPage {

    public CreatorPositionPage() {
    }

    /**
     * Функция позволяет получить до 100 лучших страниц по главной странице
     * @param request запрос с сайтом
     * @return возвращает позицию сатй {@link String} возвращает пустоту, если позиция  больше 100 или не зарегистрирован
     * */
    public String getPosition(JsonRequest request) {

        //--------------------------------------------------------------------------------
        //  Полчучение ответа http://xmlproxy.ru/ в xml формате 100 страниц по Url
        //--------------------------------------------------------------------------------
        String xmlPageDomain = YandexRequestXmlProxy.get100Domain1BySP(request);
        //--------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------
        //Получаем до 100 страниц из ответа http://xmlproxy.ru/.
        //-------------------------------------------------------------------------------------------
        ParserResponse topSite = new ParserXmlResponse();
        List<String> top10SiteUrl = topSite.getUrlSite(xmlPageDomain);
        for (int i = 0; i < top10SiteUrl.size(); i++) {
            if (top10SiteUrl.get(i).equals(request.getNameSite())) {
                int position = i+1;//Позиция сатйа
                return position + "";
            }
        }
        //-------------------------------------------------------------------------------------------
        return ""; //Сайт не зарегистрирован или более 100 позиция
    }
}
