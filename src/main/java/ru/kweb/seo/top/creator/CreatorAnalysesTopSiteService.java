package ru.kweb.seo.top.creator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.kweb.seo.top.analyses.AnalyserMetaTag;
import ru.kweb.seo.top.analyses.AnalysesAllTagsSiteModelService;
import ru.kweb.seo.top.analyses.AnalysesSEO;
import ru.kweb.seo.top.ethernet.GetterHtmlPageByUrl;
import ru.kweb.seo.top.ethernet.YandexRequestXmlProxy;
import ru.kweb.seo.top.model.json.JsonRequest;
import ru.kweb.seo.top.model.ListSite;
import ru.kweb.seo.top.model.analyses.SiteMetaTagAnalyses;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.parsers.CreateSiteModelByHtmlPage;
import ru.kweb.seo.top.parsers.ParserSite;
import ru.kweb.seo.top.parsers.ParserResponse;
import ru.kweb.seo.top.parsers.ParserXmlResponse;
import ru.kweb.seo.top.repository.ListSiteRepositoryServiceImplement;
import ru.kweb.seo.top.repository.QueueRequestService;

import javax.transaction.Transactional;
import java.util.*;

/**
 * Данный сервис работает по принципу очереди.
 * Принцип работы:
 * -Класс инициализируется в контроллере или его Distributor
 * -Как только приходит заявка с помощью метода {@link #addRequest(JsonRequest)} запрос {@link JsonRequest}
 * помещаеться в очередь {@link QueueRequestService}
 * - @Scheduled(fixedRate = {time}) вызывает метод {@link #createTopInDB()} если в очереди есть заявка.
 *       В случаи если запрос есть и частота запросов позволяет отправить запрос на http://xmlproxy.ru/
 *           идет запрос на http://xmlproxy.ru/ собирает топ 10
 *           - парсим каждую стр из топ 10
 *           - делаем анализ стр
 *           - записываем в бд
 * - В бд записывается {@link ListSite}
 *  @version 1.0
 * */

@Service
public class CreatorAnalysesTopSiteService {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(CreatorAnalysesTopSiteService.class.getName());

    //-------------------------------------------
    //  Переменные для замерки выполнения методов
    //-------------------------------------------
    private long st;
    private long en;
    private double rs;
    //-------------------------------------------

    //-------------------------------------------
    //  Репозиторий
    //-------------------------------------------
    @Autowired
    private ListSiteRepositoryServiceImplement listSiteRepository;
    //-------------------------------------------

    //-------------------------------------------
    //  Классы анализов
    //-------------------------------------------
    private AnalysesSEO seo = new AnalysesAllTagsSiteModelService();
    private AnalyserMetaTag analyserMetaTag = new AnalyserMetaTag();
    //-------------------------------------------

    //-------------------------------------------
    //  Класс работы с очередью
    //-------------------------------------------
    @Autowired
    private QueueRequestService queueRequestService;
    //-------------------------------------------

    public CreatorAnalysesTopSiteService() {
    }

    /**
     * Процедура проверяющая в бд есть ли заявка на исполнения,
     * если есть отправляет на анализ,
     * после анализа идет запись в бд
     * */
    @Scheduled(fixedRate = 2000)
    public void createTopInDB() {

        if ((queueRequestService.queueSize() != 0)) {
            JsonRequest request = queueRequestService.getRequest();
            log.info("Start analyses top: " + request.toString());
            List<SiteModel> newTop = createTop10(request);
            log.info("6. Start analyses!");
            List<SiteModel> smListAnalyses = getAnalyses(newTop, request.getSearchPhrases());
            log.info("6. Ready! <Analyses>: " + request.toString());

            ListSite listSite = new ListSite();
            for (SiteModel sm : smListAnalyses) {
                sm.setListSite(listSite);
            }
            listSite.setRegionNumber(request.getRegionNumber());
            listSite.setSearchLine(request.getSearchPhrases());
            listSite.setDate(new Date().getTime());
            listSite.setSite(new HashSet<>(smListAnalyses));
            listSite.setSiteMetaTagAnalyses(getSiteMetaTagAnalyses(listSite));
            saveRequest(listSite,request);
            log.info("End analyses top: " + request.toString());
        }
    }

    /**
     * Функция по созданию топ 10 модейлей {@link SiteModel} сайтов
     * @param request запрос
     * @return возвращает список сайтов
     **/
    private List<SiteModel> createTop10(JsonRequest request) {

        List<SiteModel> top10smList = new ArrayList<>();

        //-------------------------------------------------------------------------------------------
        //  Делаем запрос к http://xmlproxy.ru/- за топ 10
        //-------------------------------------------------------------------------------------------
        log.debug("1. Начало запроса xmlproxy");
        String top10Response = YandexRequestXmlProxy.getResponse(request);
        log.debug("1. Ready! xmlproxy");
        //-------------------------------------------------------------------------------------------


        //-------------------------------------------------------------------------------------------
        //Получаем топ 10 URL из ответа Yandex.
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        log.debug("2. Started! <Get top 10 URL>");
        ParserResponse topSite = new ParserXmlResponse();
        List<String> top10SiteUrl = topSite.getUrlTopSiteWithFilter(top10Response);
        en = System.nanoTime();
        rs = en - st;
        log.debug("2. Ready! Time get top 10 URL : " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------


        //-------------------------------------------------------------------------------------------
        //Получаем в много-поточном режиме все htmlPage
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        log.debug("3. Started! <Get top 10 html page>");
        List<String> htmlPageList = GetterHtmlPageByUrl.getTopHtml(top10SiteUrl);
        en = System.nanoTime();
        rs = en - st;
        log.debug("3. Ready! Time get top 10 html page : " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------
        // Создаем топ 10 SiteModel на основе html page
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        log.debug("4. Started! <Get top 10 SiteModel by htm pages>");
        ParserSite createSiteModelByHtmlPage = new CreateSiteModelByHtmlPage();
        int i = 0;
        for (String htmlPage : htmlPageList) {
            SiteModel sm = createSiteModelByHtmlPage.getSiteModel(htmlPage, top10SiteUrl.get(i));
            if(sm!=null)
                top10smList.add(sm);
            i++;
        }
        en = System.nanoTime();
        rs = en - st;
        log.debug("4. Ready! Time get top 10 SiteModel by htm pages: " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------
        //Дописываем позиции сайтам
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        log.debug("5. Started! <Get top 10 SiteModel + position>");
        top10smList = CreatorPositionTop10Site.creatorPosition(top10smList, top10SiteUrl);
        en = System.nanoTime();
        rs = en - st;
        log.debug("5. Ready! Time get top 10 SiteModel + position : " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------

        return top10smList;
    }

    /**
     * Функция вызывает {@link #siteModelListAnalyses(List, String)}
     * */
    private List<SiteModel> getAnalyses(List<SiteModel> newTop, String searchPhrases) {
        return siteModelListAnalyses(newTop, searchPhrases);
    }

    /**
     * Функция вызывает {@link AnalysesSEO#getAnalysesSetSiteModel(Set, String)}
     * */
    private List<SiteModel> siteModelListAnalyses(List<SiteModel> smList, String searchPhrase) {
        Set<SiteModel> siteModelSet = new HashSet<>(smList);
        return new ArrayList<>(seo.getAnalysesSetSiteModel(siteModelSet, searchPhrase));
    }

    private SiteMetaTagAnalyses getSiteMetaTagAnalyses(ListSite listSite) {
        return analyserMetaTag.createSiteMetaTagAnalyses(listSite);
    }

    /**
     * Функция сохраняет анализ в бд
     * @param listSite анализ по запросу
     * @param  request запрос
     * */
    @Transactional
    void saveRequest(ListSite listSite, JsonRequest request){
        listSiteRepository.add(listSite);
        queueRequestService.remove(request);
    }

    /**
     * Функция добавляет запрос в очередб на исполнение
     * @param  request запрос
     * */
    public void addRequest(JsonRequest request){
        queueRequestService.addRequest(request);
    }
}
