package ru.kweb.seo.top.creator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kweb.seo.top.ethernet.GetterHtmlPageByUrl;
import ru.kweb.seo.top.model.KeyWordCounter;
import ru.kweb.seo.top.model.analyses.ModelAutoSelectionPageAndWord;
import ru.kweb.seo.top.model.analyses.SiteHtmlModel;
import ru.kweb.seo.top.service.StemAnalyzer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Класс позволяет подобрать лучшие слова к странице, работает по принципу сбор всех слов со странице Html (русских)
 * возвращает часто встречаемые

 * @version 1.0
 */
public class CreatorListBestWords {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(CreatorAnalysesSiteService.class.getName());

    //-------------------------------------------
    //  Переменные для замерки выполнения методов
    //-------------------------------------------
    private long st;
    private long en;
    private double rs;
    private long start;
    private long end;
    private double result;

    //-------------------------------------------
    private List<ModelAutoSelectionPageAndWord> selections = new ArrayList<>();

    public CreatorListBestWords() {
    }

    /**
     * Функция позволяет получить до 100 лучших страниц по главной странице
     * @param urls список страниц для подбора
     * @return возвращает список моделей по кажой странцией с лучшими словами
     * */
    public List<ModelAutoSelectionPageAndWord> create(List<String> urls) {
        start = System.nanoTime();
        List<String> bestWords = new ArrayList<>();
        List<SiteHtmlModel> pages = new ArrayList<>();
        //-------------------------------------------------------------------------------------------
        // Получаем html page по url
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        pages = GetterHtmlPageByUrl.getHtmlPagesWithNamePage(urls);
        en = System.nanoTime();
        rs = en - st;
        log.debug("Get html page by url request: " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------
        // Получаем html page по url
        //-------------------------------------------------------------------------------------------
        for (SiteHtmlModel siteHtmlModel : pages) {
            try {
                bestWords = new ArrayList<>();
                List<String> allWords = StemAnalyzer.GetLex(siteHtmlModel.getHtmlPage(), false);
                Map<String, Integer> countCoincidence = getMapKeywordWithCountCoincidence(allWords);
                List<KeyWordCounter> keyWord = getSortedListTag(countCoincidence, 3);
                for (KeyWordCounter keyWordCounter : keyWord) {
                    bestWords.add(keyWordCounter.getKeywordText());
                }
            } catch (Exception e) {
                log.error("Error create best words.", siteHtmlModel.getHtmlPage());
            }

            if (bestWords != null) {
                selections.add(new ModelAutoSelectionPageAndWord(siteHtmlModel.getNamePage(), bestWords));
                log.debug("Created best words comleted!");
            } else {
                log.warn("Created best words comleted. Warn! word 0");
            }
        }
        end = System.nanoTime();
        result = end - start;
        log.debug("Get html page by url request: " + result / 1000000000);
        return selections;
    }

    /**
     * Функция ведет подсчет слов где ключ слов значение частота повторения
     * @param keywords список всех слов
     * @return возвращает мапу слов
     * */
    private Map<String, Integer> getMapKeywordWithCountCoincidence(List<String> keywords) {

        Map<String, Integer> map = new HashMap<>();
        for (String str : keywords) {
            if (!map.containsKey(str)) {
                map.put(str, 1);
            } else {
                map.put(str, map.get(str) + 1);
            }
        }
        return map;
    }

    /**
     * Функция выдает список часто поторяемых слов
     * @param map мапа всех повторяющихся слов
     * @param countLimitPhrases колличество выводимых фраз
     * @return возвращает мапу слов
     * */
    private List<KeyWordCounter> getSortedListTag(Map<String, Integer> map, int countLimitPhrases) {

        List<KeyWordCounter> metaTags = new ArrayList<>();

        for (Map.Entry entry : map.entrySet()) {
            KeyWordCounter keywordAnalyses = new KeyWordCounter((String) entry.getKey(), (int) entry.getValue());
            metaTags.add(keywordAnalyses);
        }

        return metaTags.stream()
                .sorted(((o1, o2) -> o2.getCountKeyWord() - o1.getCountKeyWord()))
                .limit(countLimitPhrases)
                .collect(Collectors.toList());
    }
}
