package ru.kweb.seo.top.creator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kweb.seo.top.ethernet.YandexRequestXmlProxy;
import ru.kweb.seo.top.model.json.JsonRequest;
import ru.kweb.seo.top.parsers.ParserResponse;
import ru.kweb.seo.top.parsers.ParserXmlResponse;

import java.util.List;

/**
 * Класс котоырй позволяет получить список самых реливатных страниц из яндекса

 * @version 1.0
 */
public class CreatorDomainPages {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(CreatorDomainPages.class.getName());

    //-------------------------------------------
    //  Переменные для замерки выполнения методов
    //-------------------------------------------
    private long st;
    private long en;
    private double rs;
    //-------------------------------------------

    public CreatorDomainPages() {
    }

    /**
     * Функция позволяет получить до 100 лучших страниц по главной странице
     * @param request запрос с сайтом
     * @return возвращает список страниц
     * */
    public List<String> getListPagesDomain(JsonRequest request) {

        //--------------------------------------------------------------------------------
        //  Полчучение ответа http://xmlproxy.ru/ в xml формате от 1 до 100 страниц по Url
        //--------------------------------------------------------------------------------
        String xmlPageDomain = YandexRequestXmlProxy.get100DomainByUrl(request);
        //--------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------
        //Получаем до 100 страниц из ответа http://xmlproxy.ru/.
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        ParserResponse topSite = new ParserXmlResponse();
        List<String> top10SiteUrl = topSite.getUrlSite(xmlPageDomain);
        en = System.nanoTime();
        rs = en - st;
        log.debug("Time get top page." + rs / 1000000000 + " Count page: " + top10SiteUrl.size());
        //-------------------------------------------------------------------------------------------
        return top10SiteUrl;
    }
}
