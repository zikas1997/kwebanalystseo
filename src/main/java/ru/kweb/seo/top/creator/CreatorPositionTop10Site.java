package ru.kweb.seo.top.creator;

import ru.kweb.seo.top.model.SiteModel;

import java.util.List;
/**
 * Класс растовляет позиции для топа

 * @version 1.0
 */
public final class CreatorPositionTop10Site {

    private CreatorPositionTop10Site() {
    }

    /**
     * Простой метод, котоырй по выгрузки топ 10 последовательно присваевает позицию сайта из топа
     * @param siteModels список моделей сайта
     * @param urls список url нужен для опредение позиции
     * @return возвращает список моделей  сайтов, только пронумерованныз
     * */
    public static List<SiteModel> creatorPosition(List<SiteModel> siteModels, List<String> urls) {
        int i = 0;
        for (String htmlPage : urls) {
            for(SiteModel siteModel: siteModels){
                if(siteModel.getNameSite().equals(htmlPage)){
                    siteModel.setPosition(i+1);
                }
            }
            i++;
        }
        return siteModels;
    }
}
