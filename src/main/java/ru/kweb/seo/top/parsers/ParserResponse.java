package ru.kweb.seo.top.parsers;

import java.util.List;

public interface ParserResponse {
    List<String> getUrlSite(String interceptedRequest);
    List<String> getUrlTopSiteWithFilter(String interceptedRequest);
}
