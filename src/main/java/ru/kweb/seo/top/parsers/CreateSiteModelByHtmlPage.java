package ru.kweb.seo.top.parsers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kweb.seo.top.creator.CreatorAnalysesTopSiteService;
import ru.kweb.seo.top.model.*;
import ru.kweb.seo.top.model.analyses.SiteMetaTagDescription;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Класс парит html страницу и собирает первоночльный информацию по главным тегам

 * @version 1.0
 */
public class CreateSiteModelByHtmlPage implements ParserSite {

    /*---Logger---*/
    private final static Logger log = LoggerFactory.getLogger(CreatorAnalysesTopSiteService.class.getName());

    private SiteModel siteModel;
    private SiteTitle title;
    private Set<SiteH> hSet;
    private SiteH h;
    private Set<SiteP> pSet;
    private SiteP p;
    private SiteMetaTagDescription siteMetaTagDescription;
    private MetaTagKeywords metaTagKeywords;

    public CreateSiteModelByHtmlPage() {
    }

    /**
     * Функция собирает информацию по тегам
     * @param pageHtml html страница
     * @param nameSiteUrl адресс сайта
     * @return возвращает первоночальную модель {@link SiteModel}
     * */
    @Override
    public SiteModel getSiteModel(String pageHtml, String nameSiteUrl) {

        try {
            siteModel = new SiteModel();
            siteModel.setNameSite(nameSiteUrl);
            Document doc = Jsoup.parse(pageHtml);
            siteModel.setTitle(getTitles(doc));
            siteModel.setH(getSetH(doc));
            siteModel.setP(getSetP(doc));
            siteModel.setSiteMetaTagDescription(getMetaTagDescription(doc));
            siteModel.setMetaTagKeywords(getMetaTagKeywords(doc));
        } catch (Exception e) {
            log.error("Exception Create Site Model. Name site: " + nameSiteUrl, e);
        }

        return siteModel;
    }

    /**
     * Функция собирает информацию по title
     * @param doc html страница
     * @return информация по title {@link SiteTitle}
     * */
    private SiteTitle getTitles(Document doc) {
        title = new SiteTitle();
        title.setTextTitle(doc.title());
        title.setSiteModel(siteModel);
        return title;
    }

    /**
     * Функция собирает информацию по всем H заголовкам
     * @param doc html страница
     * @return информация по -H-
     * */
    private Set<SiteH> getSetH(Document doc) {
        hSet = new HashSet<>();
        for (int i = 1; i < 5; i++) {//Проверяем до 5 тега h1..h4
            for (Element element : doc.body().getElementsByTag("h" + i)) {
                h = new SiteH();
                h.setNameH("h" + i);
                h.setTextH(element.text());
                h.setSiteModel(siteModel);
                hSet.add(h);
            }
        }
        return hSet;
    }

    /**
     * Функция собирает информацию по всем p заголовкам
     * @param doc html страница
     * @return информация по <p>
     * */
    private Set<SiteP> getSetP(Document doc) {
        pSet = new HashSet<>();
        for (Element element : doc.body().getElementsByTag("p")) {
            p = new SiteP();
            p.setTextP(element.text());
            p.setSiteModel(siteModel);
            pSet.add(p);
        }
        return pSet;
    }

    /**
     * Функция собирает информацию по метатегам description
     * @param doc html страница
     * @return информация description {@link SiteMetaTagDescription}
     * */
    private SiteMetaTagDescription getMetaTagDescription(Document doc) {
        siteMetaTagDescription = new SiteMetaTagDescription();
        Elements metaTegs = doc.getElementsByTag("meta");
        for (Element metaTag : metaTegs) {
            if (isExist("description", metaTag.attr("name"))) {
                siteMetaTagDescription.setDescription(true);
                siteMetaTagDescription.setTextDescription(metaTag.attr("content"));
            }
        }
        return siteMetaTagDescription;
    }

    /**
     * Функция собирает информацию по метатегам Keywords
     * @param doc html страница
     * @return информация Keywords {@link MetaTagKeywords}
     * */
    private MetaTagKeywords getMetaTagKeywords(Document doc) {
        metaTagKeywords = new MetaTagKeywords();
        Elements metaTegs = doc.getElementsByTag("meta");
        for (Element metaTag : metaTegs) {
            if (isExist("keywords", metaTag.attr("name"))) {
                metaTagKeywords.setKeywords(true);
                metaTagKeywords.setTextMetaTagKeywords(metaTag.attr("content"));
            }
        }
        return metaTagKeywords;
    }

    /**
     * Функция ищет свопадение по названием тега (регулярные выражение)
     * @param pattern образец текста
     * @param matcher сам текст
     * @return совпаденеи {@link Boolean}
     * */
    private boolean isExist(String pattern, String matcher) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(matcher);
        return m.matches();
    }

}
