package ru.kweb.seo.top.parsers;

import ru.kweb.seo.top.model.SiteModel;

public interface ParserSite {

    SiteModel getSiteModel(String pageHtml, String nameSiteUrl);
}
