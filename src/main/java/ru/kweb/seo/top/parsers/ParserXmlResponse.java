package ru.kweb.seo.top.parsers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Класс парит xml ответ от http://xmlproxy.ru/ и состовляет из него топ адресов странци

 * @version 1.0
 */
public class ParserXmlResponse implements ParserResponse {

    public ParserXmlResponse() {
    }

    /**
     * Функция без фильрта на стоп лист
     * @param interceptedRequest xml ответ
     * @return страницы
     * */
    @Override
    public List<String> getUrlSite(String interceptedRequest) {
        return parseXMLResults(interceptedRequest);
    }

    /**
     * Функция с фильрта на стоп лист
     * @param interceptedRequest xml ответ
     * @return страницы
     * */
    @Override
    public List<String> getUrlTopSiteWithFilter(String interceptedRequest) {
        return topSiteWithStopList(parseXMLResults(interceptedRequest));
    }

    /**
     * Функция разбирает xml на список адресов
     * @param xml xml ответ
     * @return страниц
     * */
    private List<String> parseXMLResults(String xml) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document xmldoc = dBuilder.parse(new InputSource(
                    new StringReader(xml)));
            xmldoc.getDocumentElement().normalize();


            List<String> results = new ArrayList<>();

            NodeList groupNodeList = xmldoc.getElementsByTagName("group");
            for (int groupId = 0; groupId < groupNodeList.getLength(); ++groupId) {
                Element groupElement = (Element) groupNodeList.item(groupId);
                NodeList docNodeList = groupElement.getElementsByTagName("doc");
                for (int docId = 0; docId < docNodeList.getLength(); ++docId) {
                    Element docElement = (Element) docNodeList.item(docId);
                    Element domainElement = (Element) docElement
                            .getElementsByTagName("url")
                            .item(0);
                    String domain = domainElement.getTextContent();

                    domain = domain.replaceAll("\\s+","");
                    results.add(domain);
                }
            }
            return results;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Функция убирает из списка страницы из стоп листа
     * @param secondList все результативные страницы
     * @return страницы вметсе со стоп листом
     * */
    private List<String> topSiteWithStopList(List<String> secondList) {
        List<String> listSite = new ArrayList<>();
        for (String site : secondList) {
            if (isStopList(site)) {
                listSite.add(site);
            }
        }
        return listSite;
    }

    /**
     * Функция проверяет есть ли сайт в Стоп листе
     * @param address все результативные страницы
     * @return сайт в стоп листе {@link Boolean}
     * */
    private boolean isStopList(String address) {
        if (!filter(".*yandex.*", address).equals("_")) {
            return false;
        }
        if (!filter(".*avito.ru.*", address).equals("_")) {
            return false;
        }
        if (!filter(".*wikipedia.*", address).equals("_")) {
            return false;
        }
        if (!filter(".*2gis.*", address).equals("_")) {
            return false;
        }
        if (!filter(".*youtube.*", address).equals("_")) {
            return false;
        }
        if (!filter(".*vk.*", address).equals("_")) {
            return false;
        }
        if (!filter(".*facebook.*", address).equals("_")) {
            return false;
        }
        if (!filter(".*ok.*", address).equals("_")) {
            return false;
        }
        if (!filter(".*odnoklassniki.*", address).equals("_")) {
            return false;
        }
        if (!filter(".*instagram.*", address).equals("_")) {
            return false;
        }
        return true;
    }

    private String filter(String pattern, String matcher) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(matcher);
        while (m.find()) {
            return matcher.substring(m.start(), m.end());
        }
        return "_";
    }
}
