package ru.kweb.seo.top.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kweb.seo.top.model.position.SitePosition;

@Repository
public interface PositionSiteRepository extends JpaRepository<SitePosition, Long>{
    boolean existsByUrlAndSearchPhrasesAndSearchRegion(String url, String searchPhrases, String searchRegion);
    SitePosition findByUrlAndSearchPhrasesAndSearchRegion(String url, String searchPhrases, String searchRegion);
}
