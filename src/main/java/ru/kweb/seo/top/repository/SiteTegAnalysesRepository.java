package ru.kweb.seo.top.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kweb.seo.top.model.SiteTagAnalyses;

@Repository
public interface SiteTegAnalysesRepository extends JpaRepository<SiteTagAnalyses, Long> {
}
