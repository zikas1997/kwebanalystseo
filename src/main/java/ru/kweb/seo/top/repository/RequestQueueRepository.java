package ru.kweb.seo.top.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kweb.seo.top.model.json.JsonRequest;

@Repository
public interface RequestQueueRepository extends JpaRepository<JsonRequest,Long>{
    boolean existsBySearchPhrasesAndRegionNumber(String s,int i);
    JsonRequest findFirstByDateIsNotNullOrderByDateAsc();
}
