package ru.kweb.seo.top.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kweb.seo.top.model.json.JsonRequest;


@Service
public class QueueRequestService {

    /*---Logger---*/
    private final static Logger log = LoggerFactory.getLogger(QueueRequestService.class.getName());
    @Autowired
    RequestQueueRepository repository;

    public void addRequest(JsonRequest request) {
       if(!repository.existsBySearchPhrasesAndRegionNumber(request.getSearchPhrases(),request.getRegionNumber())){
           repository.save(request);
       }
    }

    public JsonRequest getRequest() {
        return repository.findFirstByDateIsNotNullOrderByDateAsc();
    }

    public long queueSize() {
       return repository.count();
    }

    public void remove(JsonRequest request) {
        repository.delete(request);
    }
}
