package ru.kweb.seo.top.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kweb.seo.top.model.ListSite;

@Repository
public interface ListSiteRepository extends JpaRepository<ListSite, Long> {
    Iterable<ListSite> findAllBySearchLineAndRegionNumber(String searchLine, int regionNumber);
    ListSite findBySearchLineAndRegionNumber(String searchLine, int regionNumber);
    boolean existsBySearchLineAndRegionNumber(String searchLine, int regionNumber);
    boolean existsAllBySearchLineAndRegionNumber(String searchLine, int regionNumber);
    long countBySearchLineAndRegionNumber(String searchLine, int regionNumber);
}
