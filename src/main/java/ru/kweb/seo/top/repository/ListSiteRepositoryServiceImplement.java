package ru.kweb.seo.top.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kweb.seo.top.model.ListSite;

import java.util.List;
import java.util.stream.Stream;

@Service
public class ListSiteRepositoryServiceImplement {

    @Autowired
    ListSiteRepository repository;

    public ListSiteRepositoryServiceImplement() {
    }

    public void add(ListSite listSite) {
        repository.save(listSite);
    }

    public boolean existTop(String searchPhrases, int region) {
        if (repository.existsAllBySearchLineAndRegionNumber(searchPhrases, region)) {
            return true;
        }
        return false;
    }

    public ListSite getTop(String searchPhrases, int region) {
        if (repository.existsBySearchLineAndRegionNumber(searchPhrases, region)) {
            return repository.findBySearchLineAndRegionNumber(searchPhrases, region);
        }
        return null;
    }

    public boolean existAnalyse(String searchPhrases, int region){
        if (repository.existsBySearchLineAndRegionNumber(searchPhrases, region)) {
            return true;
        }
        return false;
    }
    public Stream<ListSite> getAll() {
        return repository.findAll().stream();
    }

    public void addAll(List<ListSite> listSites) {
        repository.save(listSites);
    }

}
