package ru.kweb.seo.top.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import ru.kweb.seo.top.model.SiteModel;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
/**
 * Класс отправлятель почты м вложеным excel файлом.
 * @version 1.0
 */
@Component
public class EmailSender {


    @Autowired
    public JavaMailSender emailSender;

    public EmailSender() {
    }
    /**
     * Процедура отправляем письмо на почту
     * @param to кому или почта получателя
     * @param subject тема письма
     * @param text текст письма
     * @param pathToAttachment путь к прекрепленному файлу
     * */
    public void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment) {

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = null;

        try {
            helper = new MimeMessageHelper(message, true);

            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);

            FileSystemResource file
                    = new FileSystemResource(new File(pathToAttachment));
            helper.addAttachment("poseosh.xls", file);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        emailSender.send(message);
        System.out.println("Сообщение отправлено");
    }
}
