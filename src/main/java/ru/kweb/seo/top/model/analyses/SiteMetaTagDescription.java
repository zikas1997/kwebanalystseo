package ru.kweb.seo.top.model.analyses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.kweb.seo.top.model.SiteModel;

import javax.persistence.*;
/**
 * Класс сущность хрнящий метотег посайту

 * @version 1.0
 */
@Entity
public class SiteMetaTagDescription {

    /** id - c автоматическим созданием, нужно для БД*/
    @Id
    @GeneratedValue
    @JsonIgnore
    private long id;

    /** Текст мета тэга Description */
    @Transient
    private String textDescription;

    /** Наличие на странице мета тэга Description */
    private boolean isDescription;

    /** Связь с сайтом {@link SiteModel}*/
    @OneToOne(mappedBy = "siteMetaTagDescription")
    @JsonIgnore
    private SiteModel siteModel;

    public SiteMetaTagDescription() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }

    public boolean isDescription() {
        return isDescription;
    }

    public void setDescription(boolean description) {
        isDescription = description;
    }

    public SiteModel getSiteModel() {
        return siteModel;
    }

    public void setSiteModel(SiteModel siteModel) {
        this.siteModel = siteModel;
    }
}
