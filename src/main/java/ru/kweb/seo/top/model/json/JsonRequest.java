package ru.kweb.seo.top.model.json;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Класс модель представленеи json request

 * @version 1.0
 */
@Entity
public class JsonRequest {

    /** id - c автоматическим созданием, нужно для БД*/
    @Id
    @GeneratedValue
    private long id;

    /** Поисковая фраза */
    private String searchPhrases;

    /** Название страницы сата */
    private String nameSite;

    /** Поисковой регион */
    private int regionNumber;

    /** Дата создания запроса */
    private long date;

    public JsonRequest() {
    }

    public JsonRequest(String searchPhrases, int regionNumber) {
        this.searchPhrases = searchPhrases;
        this.regionNumber = regionNumber;
    }

    public JsonRequest(String searchPhrases, String nameSite, int regionNumber) {
        this.searchPhrases = searchPhrases;
        this.nameSite = nameSite;
        this.regionNumber = regionNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getSearchPhrases() {
        return searchPhrases;
    }

    public void setSearchPhrases(String searchPhrases) {
        this.searchPhrases = searchPhrases;
    }

    public String getNameSite() {
        return nameSite;
    }

    public void setNameSite(String nameSite) {
        this.nameSite = nameSite;
    }

    public int getRegionNumber() {
        return regionNumber;
    }

    public void setRegionNumber(int regionNumber) {
        this.regionNumber = regionNumber;
    }


    @Override
    public String toString() {
        return "JsonRequest{" +
                "searchPhrases='" + searchPhrases + '\'' +
                ", nameSite='" + nameSite + '\'' +
                ", regionNumber=" + regionNumber +
                '}';
    }
}
