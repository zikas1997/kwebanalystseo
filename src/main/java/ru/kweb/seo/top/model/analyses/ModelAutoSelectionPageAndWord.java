package ru.kweb.seo.top.model.analyses;

import java.util.List;

/**
 * Класс модель, храянщаяя самые релевантыные слова по странцице запроса

 * @version 1.0
 */
public class ModelAutoSelectionPageAndWord {

    /** название станицы */
    private String page;

    /** список часто встречающихся слов по странице*/
    private List<String> words;

    public ModelAutoSelectionPageAndWord() {
    }

    /**
     * Конструктор класса
     * @param page название станицы
     * @param words список часто встречающихся слов по странице
     * */
    public ModelAutoSelectionPageAndWord(String page, List<String> words) {
        this.page = page;
        this.words = words;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }
}
