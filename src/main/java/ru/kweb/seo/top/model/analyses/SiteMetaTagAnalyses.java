package ru.kweb.seo.top.model.analyses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import ru.kweb.seo.top.model.ListSite;

import javax.persistence.*;
import java.util.Set;

/**
 * Класс модель, хранящий анализцы по метаттегам

 * @version 1.0
 */
@Entity
public class SiteMetaTagAnalyses {

    /** id - c автоматическим созданием, нужно для БД*/
    @Id
    @GeneratedValue
    private long id;

    /** Множество {@link KeywordAnalyses} */
    @OneToMany(mappedBy = "siteMetaTagAnalyses1",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<KeywordAnalyses> keywordAnalyses;

    /** Связь с глобальной моделью {@link ListSite} */
    @OneToOne(mappedBy = "siteMetaTagAnalyses")
    @JsonIgnore
    private ListSite listSite;

    public SiteMetaTagAnalyses() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ListSite getListSite() {
        return listSite;
    }

    public void setListSite(ListSite listSite) {
        this.listSite = listSite;
    }

    public Set<KeywordAnalyses> getKeywordAnalyses() {
        return keywordAnalyses;
    }

    public void setKeywordAnalyses(Set<KeywordAnalyses> keywordAnalyses) {
        this.keywordAnalyses = keywordAnalyses;
    }

}
