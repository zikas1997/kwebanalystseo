package ru.kweb.seo.top.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import ru.kweb.seo.top.model.analyses.SiteMetaTagAnalyses;

import javax.persistence.*;
import java.util.Set;

/**
 * Класс глобальная модель. Являеться основной сущностью для хранения данных по анализу

 * @version 1.0
 */
@Entity
public class ListSite {

    /** id - c автоматическим созданием, нужно для БД*/
    @Id
    @GeneratedValue
    private long idListSite;

    /** поисковая фраза */
    private String searchLine;

    /** Множество сайтов от 0 до 10 - топ сатов по запросу*/
    @OneToMany(mappedBy = "listSite",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<SiteModel> site;

    /** дата создания анализа*/
    @JsonIgnore
    private long date;

    /** регион по которму совершался запрос (число)*/
    private int regionNumber;

    /** дополнительный анализ для списка сайтов по запросу*/
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "siteMetaTagAnalyses_id")
    private SiteMetaTagAnalyses siteMetaTagAnalyses;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public ListSite() {
    }

    public int getRegionNumber() {
        return regionNumber;
    }

    public void setRegionNumber(int regionNumber) {
        this.regionNumber = regionNumber;
    }

    public Set<SiteModel> getSite() {
        return site;
    }

    public void setSite(Set<SiteModel> site) {
        this.site = site;
    }

    public long getIdListSite() {
        return idListSite;
    }

    public void setIdListSite(long idListSite) {
        this.idListSite = idListSite;
    }

    public String getSearchLine() {
        return searchLine;
    }

    public void setSearchLine(String searchLine) {
        this.searchLine = searchLine;
    }

    public SiteMetaTagAnalyses getSiteMetaTagAnalyses() {
        return siteMetaTagAnalyses;
    }

    public void setSiteMetaTagAnalyses(SiteMetaTagAnalyses siteMetaTagAnalyses) {

        this.siteMetaTagAnalyses = siteMetaTagAnalyses;
    }
}
