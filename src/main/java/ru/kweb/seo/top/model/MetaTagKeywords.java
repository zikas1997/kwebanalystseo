package ru.kweb.seo.top.model;

/**
 * Класс модель, хранящий информацию о

 * @version 1.0
 */
public class MetaTagKeywords {

    /** значение тега */
    private String textMetaTagKeywords;

    /** наличие тега */
    private boolean isKeywords;

    public String getTextMetaTagKeywords() {
        return textMetaTagKeywords;
    }

    public void setTextMetaTagKeywords(String textMetaTagKeywords) {
        this.textMetaTagKeywords = textMetaTagKeywords;
    }

    public boolean isKeywords() {
        return isKeywords;
    }

    public void setKeywords(boolean keywords) {
        isKeywords = keywords;
    }
}
