package ru.kweb.seo.top.model.analyses;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

/**
 * Класс модель, являеться временым хранилищем для определение лучших фраз из списка ключевых фраз

 * @version 1.0
 */
@Entity
public class KeywordAnalyses {

    /** id - c автоматическим созданием, нужно для БД*/
    @Id
    @GeneratedValue
    private long id;

    /** Ключивая фраза */
    private String keywordText;

    /** Колличестов встречающихся фраз */
    private int count;

    /** Связь один ко многим с {@link SiteMetaTagAnalyses} */
    @ManyToOne
    @JoinColumn(name = "siteMetaTagAnalyses1_id")
    @JsonBackReference
    private SiteMetaTagAnalyses siteMetaTagAnalyses1;


    public KeywordAnalyses() {
    }

    /**
     *Констуктр с параметрами
     * @param keywordText ключивая фраза
     * @param count колличестов встречающихся фраз
     * */
    public KeywordAnalyses(String keywordText, int count) {
        this.keywordText = keywordText;
        this.count = count;
    }




    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SiteMetaTagAnalyses getSiteMetaTagAnalyses1() {
        return siteMetaTagAnalyses1;
    }

    public void setSiteMetaTagAnalyses1(SiteMetaTagAnalyses siteMetaTagAnalyses1) {
        this.siteMetaTagAnalyses1 = siteMetaTagAnalyses1;
    }

    public String getKeywordText() {
        return keywordText;
    }

    public void setKeywordText(String keywordText) {
        this.keywordText = keywordText;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
