package ru.kweb.seo.top.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Класс модель представление тега-title-

 * @version 1.0
 */
public class SiteTitle {

    /** значение тэга*/
    private String textTitle;

    /** Связь с {@link SiteModel} */
    @JsonIgnore
    private SiteModel siteModel;

    public SiteTitle() {
    }

    public SiteModel getSiteModel() {
        return siteModel;
    }

    public void setSiteModel(SiteModel siteModel) {
        this.siteModel = siteModel;
    }

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle;
    }
}
