package ru.kweb.seo.top.model.report;

/**
 * Класс модель, представляющий полный анализ однородных тэгов

 * @version 1.0
 */
public class TagModel{

    /** Названия тэгов */
    private String nameTag;

    /** Колличество тэгов */
    private double count = 0;

    /** идеальное значение символов в тэге */
    private double countChar = 0;

    /** идеальное значение слов в тэгах */
    private double countWord = 0;

    /** идеальное значение ключивых слов в тэгах */
    private double percentSearchPhrases = 0;

    /** Коридор для символов (Минимальный) */
    private double minChar = 99999;

    /** Коридор для символов (Максимальный) */
    private double maxChar = 0;

    /** Коридор для слов (Минимальный) */
    private double minWord = 99999;

    /** Коридор для слов (Максимальный) */
    private double maxWord = 0;

    /** Коридор для процент вхождения (Минимальный) */
    private double minPercentSearchPhrases = 100;

    /** Коридор для процент вхождения (Максимальный) */
    private double maxPercentSearchPhrases = 0;

    /** Превое вхождение ключевой фразы*/
    private double firstEntrySearchPhrases = 0;

    /** Колличество ключивых слов в тэгах */
    private double countInputPhrases;


    public TagModel() {
    }


    public String getNameTag() {
        return nameTag;
    }

    public void setNameTag(String nameTag) {
        this.nameTag = nameTag;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public double getCountChar() {
        return countChar;
    }

    public void setCountChar(double countChar) {
        this.countChar = countChar;
    }

    public double getCountWord() {
        return countWord;
    }

    public void setCountWord(double countWord) {
        this.countWord = countWord;
    }

    public double getPercentSearchPhrases() {
        return percentSearchPhrases;
    }

    public void setPercentSearchPhrases(double percentSearchPhrases) {
        this.percentSearchPhrases = percentSearchPhrases;
    }

    public double getFirstEntrySearchPhrases() {
        return firstEntrySearchPhrases;
    }

    public void setFirstEntrySearchPhrases(double firstEntrySearchPhrases) {
        this.firstEntrySearchPhrases = firstEntrySearchPhrases;
    }

    public double getCountInputPhrases() {
        return countInputPhrases;
    }

    public void setCountInputPhrases(double countInputPhrases) {
        this.countInputPhrases = countInputPhrases;
    }

    public double getMinChar() {
        return minChar;
    }

    public void setMinChar(double minChar) {
        this.minChar = minChar;
    }

    public double getMaxChar() {
        return maxChar;
    }

    public void setMaxChar(double maxChar) {
        this.maxChar = maxChar;
    }

    public double getMinWord() {
        return minWord;
    }

    public void setMinWord(double minWord) {
        this.minWord = minWord;
    }

    public double getMaxWord() {
        return maxWord;
    }

    public void setMaxWord(double maxWord) {
        this.maxWord = maxWord;
    }

    public double getMinPercentSearchPhrases() {
        return minPercentSearchPhrases;
    }

    public void setMinPercentSearchPhrases(double minPercentSearchPhrases) {
        this.minPercentSearchPhrases = minPercentSearchPhrases;
    }

    public double getMaxPercentSearchPhrases() {
        return maxPercentSearchPhrases;
    }

    public void setMaxPercentSearchPhrases(double maxPercentSearchPhrases) {
        this.maxPercentSearchPhrases = maxPercentSearchPhrases;
    }
}

