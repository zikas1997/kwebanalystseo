package ru.kweb.seo.top.model.analyses;

/**
 * Класс модель, хранянщий html представление страницы

 * @version 1.0
 */
public class SiteHtmlModel {

    /** адрес страницы*/
    private String namePage;

    /** html представленеи страницы*/
    private String htmlPage;

    public SiteHtmlModel() {
    }

    /**
     * Конструктор класса
     * @param namePage адрес страницы
     * @param htmlPage html представленеи страницы
     * */
    public SiteHtmlModel(String namePage, String htmlPage) {
        this.namePage = namePage;
        this.htmlPage = htmlPage;
    }

    public String getNamePage() {
        return namePage;
    }

    public void setNamePage(String namePage) {
        this.namePage = namePage;
    }

    public String getHtmlPage() {
        return htmlPage;
    }

    public void setHtmlPage(String htmlPage) {
        this.htmlPage = htmlPage;
    }
}
