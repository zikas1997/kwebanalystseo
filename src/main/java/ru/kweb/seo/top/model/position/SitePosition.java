package ru.kweb.seo.top.model.position;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Класс модель нужна для определение позиции храниться в базе 2 недели.

 * @version 1.0
 */
@Entity
public class SitePosition {

    /** id - c автоматическим созданием, нужно для БД*/
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /** страница сатйа */
    private String url;

    /** Поисковая фраза */
    private String searchPhrases;

    /** поисковой регион */
    private String searchRegion;

    /** позиция страницы */
    private String positionSite;

    /** Последнее изменение позиции сайта*/
    private long lastTime;

    public SitePosition() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSearchPhrases() {
        return searchPhrases;
    }

    public void setSearchPhrases(String searchPhrases) {
        this.searchPhrases = searchPhrases;
    }

    public String getSearchRegion() {
        return searchRegion;
    }

    public void setSearchRegion(String searchRegion) {
        this.searchRegion = searchRegion;
    }

    public String getPositionSite() {
        return positionSite;
    }

    public void setPositionSite(String positionSite) {
        this.positionSite = positionSite;
    }

    public long getLastTime() {
        return lastTime;
    }

    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
    }
}
