package ru.kweb.seo.top.model;

/**
 * Класс модель нужна для подсчета слов.

 * @version 1.0
 */
public class KeyWordCounter {

    /** Слово */
    private String keywordText;
    /** Колличество слов*/
    private int countKeyWord;

    /**Коструктор класса*/
    public KeyWordCounter() {
    }

    /**
     * Конструктор - создание нового объекта с определенными значениями
     * @param keywordText - слово
     * @param countKeyWord - колличество слов
     * @see KeyWordCounter#KeyWordCounter(String, int)
     */
    public KeyWordCounter(String keywordText, int countKeyWord) {
        this.keywordText = keywordText;
        this.countKeyWord = countKeyWord;
    }

    public String getKeywordText() {
        return keywordText;
    }

    public void setKeywordText(String keywordText) {
        this.keywordText = keywordText;
    }

    public int getCountKeyWord() {
        return countKeyWord;
    }

    public void setCountKeyWord(int countKeyWord) {
        this.countKeyWord = countKeyWord;
    }

    @Override
    public String toString() {
        return "KeywordAnalyses{" +
                "keywordText='" + keywordText + '\'' +
                ", countKeyWord=" + countKeyWord +
                '}';
    }
}
