package ru.kweb.seo.top.model.json;

/**
 * Класс модель представление Json body для запроса {@link ru.kweb.seo.top.controller.AnalyseController#report(String, String, String, String)}

 * @version 1.0
 */
public class JsonBodyReport {

    /** Список сайтов */
    private String[] phrases;

    public JsonBodyReport() {
    }

    public JsonBodyReport(String[] phrases) {
        this.phrases = phrases;
    }

    public String[] getPhrases() {
        return phrases;
    }

    public void setPhrases(String[] phrases) {
        this.phrases = phrases;
    }
}
