package ru.kweb.seo.top.model.report;

import java.util.List;

/**
 * Класс модель представляеться ввиде структуры сайта

 * @version 1.0
 */
public class ModelAnalyserTop {

    /** Ключивая фраза*/
    private String phrases;

    /** Ключивой регион */
    private int region;

    /** Спиоок анализа однородных тэгов {@link TagModel}*/
    private List<TagModel> tags;

    public ModelAnalyserTop() {
    }

    public String getPhrases() {
        return phrases;
    }

    public void setPhrases(String phrases) {
        this.phrases = phrases;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public List<TagModel> getTags() {
        return tags;
    }

    public void setTags(List<TagModel> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "ModelAnalyserTop{" +
                "phrases='" + phrases + '\'' +
                ", region=" + region +
                ", tags=" + tags +
                '}';
    }
}
