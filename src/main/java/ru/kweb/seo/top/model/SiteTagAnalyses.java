package ru.kweb.seo.top.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Класс модель, хранящий все проанализированные теги по сайту

 * @version 1.0
 */
@Entity
public class SiteTagAnalyses implements Serializable {

    /** id - c автоматическим созданием, нужно для БД*/
    @Id
    @GeneratedValue
    @JsonIgnore
    private long idTeg;

    /** Названия тега: H1..H4, p, title */
    private String tegName;

    /** Колличество слов в теге */
    private int countWord;

    /** Колличесвто символов*/
    private int countChar;

    /** Колличесвто ключевых слов в теге */
    private double countInputPhrase;

    /** Позиция преого вхождени ключевого слово*/
    private int positionFirstPhrasesWord;

    /** Связь с {@link SiteModel} */
    @ManyToOne
    @JoinColumn(name = "siteTegAnalyses_id")
    @JsonBackReference
    private SiteModel siteModel;

    public SiteTagAnalyses() {
    }

    public long getIdTeg() {
        return idTeg;
    }

    public void setIdTeg(long idTeg) {
        this.idTeg = idTeg;
    }

    public String getTegName() {
        return tegName;
    }

    public void setTegName(String tegName) {
        this.tegName = tegName;
    }

    public int getCountWord() {
        return countWord;
    }

    public void setCountWord(int countWord) {
        this.countWord = countWord;
    }

    public int getCountChar() {
        return countChar;
    }

    public void setCountChar(int countChar) {
        this.countChar = countChar;
    }

    public double getCountInputPhrase() {
        return countInputPhrase;
    }

    public void setCountInputPhrase(double countInputPhrase) {
        this.countInputPhrase = countInputPhrase;
    }

    public SiteModel getSiteModel() {
        return siteModel;
    }

    public void setSiteModel(SiteModel siteModel) {
        this.siteModel = siteModel;
    }

    public int getPositionFirstPhrasesWord() {
        return positionFirstPhrasesWord;
    }

    public void setPositionFirstPhrasesWord(int positionFirstPhrasesWord) {
        this.positionFirstPhrasesWord = positionFirstPhrasesWord;
    }
}
