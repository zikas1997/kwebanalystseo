package ru.kweb.seo.top.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Класс модель представление тега <p>

 * @version 1.0
 */
public class SiteP {

    /** значение тэга*/
    private String textP;

    /** Связь с {@link SiteModel} */
    @JsonIgnore
    private SiteModel siteModel;

    public SiteModel getSiteModel() {
        return siteModel;
    }

    public void setSiteModel(SiteModel siteModel) {
        this.siteModel = siteModel;
    }

    public String getTextP() {
        return textP;
    }

    public void setTextP(String textP) {
        this.textP = textP;
    }
}
