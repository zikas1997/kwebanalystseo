package ru.kweb.seo.top.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Класс модель представление тега "Hn"

 * @version 1.0
 */
public class SiteH {

    /** Названия тега H1..H4 */
    private String nameH;

    /** содержания тега */
    private String textH;

    /** Связь с {@link SiteModel} */
    @JsonIgnore
    private SiteModel siteModel;

    public String getNameH() {
        return nameH;
    }

    public void setNameH(String nameH) {
        this.nameH = nameH;
    }

    public String getTextH() {
        return textH;
    }

    public void setTextH(String textH) {
        this.textH = textH;
    }

    public SiteModel getSiteModel() {
        return siteModel;
    }

    public void setSiteModel(SiteModel siteModel) {
        this.siteModel = siteModel;
    }
}


