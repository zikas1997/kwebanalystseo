package ru.kweb.seo.top.model;

import com.fasterxml.jackson.annotation.*;
import ru.kweb.seo.top.configuration.View;
import ru.kweb.seo.top.model.analyses.SiteMetaTagDescription;
import ru.kweb.seo.top.model.report.ModelAnalyserTop;

import javax.persistence.*;
import java.util.Set;
/**
 * Класс модель представляеться ввиде структуры сайта

 * @version 1.0
 */
@Entity
public class SiteModel {

    /** id - c автоматическим созданием, нужно для БД*/
    @Id
    @GeneratedValue
    @JsonIgnore
    private long idSiteModel;

    /** название страницы сайта */
    private String nameSite;

    /** позиция страцниы сайта */
    private int position;

    /** время закгрузки сайта(отклик) */
    private long loadTime;

    /** Свзяь с глобальной моделью {@link ListSite} */
    @ManyToOne
    @JoinColumn(name = "listSite_id")
    @JsonBackReference
    private ListSite listSite;

    /** Свзяь многие к одному таблица {@link SiteTagAnalyses} */
    @OneToMany(mappedBy = "siteModel",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<SiteTagAnalyses> siteTagAnalyses;

    /** Свзяь один к одному таблица {@link SiteTitle} */
    @Transient
    @JsonView(View.SiteModel.class)
    private SiteTitle title;

    /** Свзяь многие к одному таблица {@link SiteH} */
    @Transient
    @JsonView(View.SiteModel.class)
    private Set<SiteH> h;

    /** Свзяь многие к одному таблица {@link SiteP} */
    @Transient
    @JsonView(View.SiteModel.class)
    private Set<SiteP> p;

    /** Анализ всего сайта */
    @Transient
    private ModelAnalyserTop modelAnalyserTop;

    /** Анализ -meta name="description"- */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "siteMetaTagDescription_id")
    @JsonIgnore
    private SiteMetaTagDescription siteMetaTagDescription;

    /** Анализ   */
    @Transient
    private MetaTagKeywords metaTagKeywords;

    public SiteModel() {
    }

    public ModelAnalyserTop getModelAnalyserTop() {
        return modelAnalyserTop;
    }

    public void setModelAnalyserTop(ModelAnalyserTop modelAnalyserTop) {
        this.modelAnalyserTop = modelAnalyserTop;
    }

    public long getIdSiteModel() {
        return idSiteModel;
    }

    public void setIdSiteModel(long idSiteModel) {
        this.idSiteModel = idSiteModel;
    }

    public String getNameSite() {
        return nameSite;
    }

    public void setNameSite(String nameSite) {
        this.nameSite = nameSite;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ListSite getListSite() {
        return listSite;
    }

    public void setListSite(ListSite listSite) {
        this.listSite = listSite;
    }

    public SiteTitle getTitle() {
        return title;
    }

    public void setTitle(SiteTitle title) {
        this.title = title;
    }

    public Set<SiteH> getH() {
        return h;
    }

    public void setH(Set<SiteH> h) {
        this.h = h;
    }

    public Set<SiteP> getP() {
        return p;
    }

    public void setP(Set<SiteP> p) {
        this.p = p;
    }

    public Set<SiteTagAnalyses> getSiteTagAnalyses() {
        return siteTagAnalyses;
    }

    public void setSiteTagAnalyses(Set<SiteTagAnalyses> siteTagAnalyses) {
        this.siteTagAnalyses = siteTagAnalyses;
    }

    public SiteMetaTagDescription getSiteMetaTagDescription() {
        return siteMetaTagDescription;
    }

    public void setSiteMetaTagDescription(SiteMetaTagDescription siteMetaTagDescription) {
        this.siteMetaTagDescription = siteMetaTagDescription;
    }

    public MetaTagKeywords getMetaTagKeywords() {
        return metaTagKeywords;
    }

    public void setMetaTagKeywords(MetaTagKeywords metaTagKeywords) {
        this.metaTagKeywords = metaTagKeywords;
    }

    public long getLoadTime() {
        return loadTime;
    }

    public void setLoadTime(long loadTime) {
        this.loadTime = loadTime;
    }
}
