package ru.kweb.seo.top.json;

public class JsonBodyRequest {

    private JsonConvector jsonConvector = new JsonConvector();

    public JsonBodyRequest() {
    }

    public String[] allSearchPhrases(String json){
        return jsonConvector.deserializeJsonBodyReport(json);
    }

}
