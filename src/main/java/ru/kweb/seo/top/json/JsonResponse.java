package ru.kweb.seo.top.json;

import ru.kweb.seo.top.model.ListSite;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.model.SiteTagAnalyses;
import ru.kweb.seo.top.model.analyses.ModelAutoSelectionPageAndWord;
import ru.kweb.seo.top.model.report.ModelAnalyserTop;

import java.util.List;
/**
 * Класс заниаеться вызовов методов для конвертация json

 * @version 1.0
 */
public class JsonResponse {

    private JsonConvector jsonConvector = new JsonConvector();

    public JsonResponse() {
    }

    public String getYes() {
        return "{\"response\" : \"yes\"}";
    }

    public String getNo() {
        return "{\"response\" : \"no\"}";
    }

    public String getTopAnalyses(ListSite ls) {
        return jsonConvector.serializeJsonListSite(ls);
    }

    public String getAnalysesSite(SiteModel sm) {
        return jsonConvector.serializeJsonSite(sm);
    }

    public String exceptionJson() {
        return "{\"response\" : \"Exception json.\"}";
    }

    public String getTagAnalyses(SiteTagAnalyses tagAnalyses) {
        return jsonConvector.serializeJsonSiteTagAnalyses(tagAnalyses);
    }

    public String getModelAutoSelection(List<ModelAutoSelectionPageAndWord> selections) {
        return jsonConvector.serializeModelAutoSelection(selections);
    }

    public String getPosition(String position, String id) {
        return "{\"id\":\"" + id + "\"," +
                "\"response\" : \"" + position + "\"}";
    }
    public String getTitle(String title, String id) {
        if (title != null)
            return "{\"id\":\"" + id + "\"," +
                    "\"response\" : \"" + title + "\"}";
        return getNo();
    }

    public String getReport(ModelAnalyserTop modelAnalyserTop){
        return jsonConvector.serializeReport(modelAnalyserTop);
    }
}
