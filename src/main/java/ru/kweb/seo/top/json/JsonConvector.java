package ru.kweb.seo.top.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kweb.seo.top.model.ListSite;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.model.SiteTagAnalyses;
import ru.kweb.seo.top.model.analyses.ModelAutoSelectionPageAndWord;
import ru.kweb.seo.top.model.json.JsonBodyReport;
import ru.kweb.seo.top.model.report.ModelAnalyserTop;

import java.io.IOException;
import java.util.List;
/**
 * Класс заниаеться серилизацией или десиреализацией json объектов

 * @version 1.0
 */
public class JsonConvector {

    /*---Logger---*/
    private final static Logger log = LoggerFactory.getLogger(JsonConvector.class.getName());

    private ObjectMapper objectMapper = new ObjectMapper();
    private String result = "";

    public JsonConvector() {
    }

    public String serializeJsonSite(SiteModel siteModel) {
        try {
            result = objectMapper.writeValueAsString(siteModel);
        } catch (JsonProcessingException e) {
            log.error("Exception serialize json \"site model\": ", e);
        }
        return result;
    }

    public String serializeJsonListSite(ListSite site) {
        try {
            result = objectMapper.writeValueAsString(site);
        } catch (JsonProcessingException e) {
            log.error("Exception serialize json \"list site\":  ", e);
        }
        return result;
    }

    public SiteModel deserializeJsonSite(String siteModel) {
        SiteModel sm = null;
        try {
            sm = objectMapper.readValue(siteModel, SiteModel.class);
        } catch (IOException e) {
            log.error("Exception deserialize json \"site model\":  ", e);
        }
        return sm;
    }
    public String[] deserializeJsonBodyReport(String json) {
        String[] strings = null;
        try {
           strings = objectMapper.readValue(json, JsonBodyReport.class).getPhrases();
        } catch (IOException e) {
            log.error("Exception deserialize json \"site model\":  ", e);
        }
        return strings;
    }

    public String serializeJsonSiteTagAnalyses(SiteTagAnalyses siteTagAnalyses){
        try {
            result = objectMapper.writeValueAsString(siteTagAnalyses);
        } catch (JsonProcessingException e) {
            log.error("Exception serialize json \"Site Tag Analyses\": ", e);
        }
        return result;
    }

    public String serializeList(List<String> pagesSite){
        try {
            result = objectMapper.writeValueAsString(pagesSite);
        } catch (JsonProcessingException e) {
            log.error("Exception serialize json \"List Pages Site\": ", e);
        }
        return result;
    }

    public String serializeModelAutoSelection(List<ModelAutoSelectionPageAndWord> selections){
        try {
            result = objectMapper.writeValueAsString(selections);
        } catch (JsonProcessingException e) {
            log.error("Exception serialize json \"List Pages Site\": ", e);
        }
        return result;
    }

    public String serializeReport(ModelAnalyserTop modelAnalyserTop){
        try {
            result = objectMapper.writeValueAsString(modelAnalyserTop);
        } catch (JsonProcessingException e) {
            log.error("Exception serialize json \"List Pages Site\": ", e);
        }
        return result;
    }
}
