package ru.kweb.seo.top.analyses;

import ru.kweb.seo.top.model.SiteH;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.model.SiteP;
import ru.kweb.seo.top.model.SiteTagAnalyses;

import java.util.HashSet;
import java.util.Set;

/**
 * Класс собирает первичный анализ в {@link SiteModel} (сырые данные)

 * @version 1.0
 */
public class AnalyserAllTagsSiteModel {

    /** Возвращяемая модель */
    private SiteModel sm;

    public AnalyserAllTagsSiteModel() {
    }

    /**
     *  Функция делает сырой анализ и возвращает рзультат
     *  Вызывает фунцию для анализа {@link #getAnalysesTag(String, String, String)}
     *  @param siteModel модель с некоторыми данными для анализа
     *  @param searchPhrase поисковая фраза
     *  @return возвращает модель {@link #sm}
     * */
    public SiteModel createTagAnalyses(SiteModel siteModel, String searchPhrase) {

        sm = siteModel;
        Set<SiteTagAnalyses> analyses = new HashSet<>();

        //Title
        try {
            analyses.add(getAnalysesTag("title", sm.getTitle().getTextTitle(), searchPhrase));
        } catch (NullPointerException  e) {
            System.out.println(sm);
        }
        //SetP
        Set<SiteP> pSet = sm.getP();
        for (SiteP aPSet : pSet) {
            analyses.add(getAnalysesTag("p", aPSet.getTextP(), searchPhrase));
        }

        //SetH
        Set<SiteH> hSet = sm.getH();
        for (SiteH h : hSet) {
            analyses.add(getAnalysesTag(h.getNameH(), h.getTextH(), searchPhrase));
        }
        sm.setSiteTagAnalyses(analyses);

        return sm;

    }

    /**
     *  Функция составляет анализ, инициализируя и вызывая методы класса {@link AnalyserOneTag}
     *  @param nameTeg имя тэга
     *  @param tagText текст тэга
     *  @param searchPhrase поисковая фраза
     *  @return возвращает модель {@link SiteTagAnalyses}
     * */
    private SiteTagAnalyses getAnalysesTag(String nameTeg, String tagText, String searchPhrase) {

        AnalyserOneTag oneTeg = new AnalyserOneTag(tagText, searchPhrase);
        SiteTagAnalyses siteTagAnalyses = new SiteTagAnalyses();
        siteTagAnalyses.setTegName(nameTeg);
        siteTagAnalyses.setSiteModel(sm);
        siteTagAnalyses.setCountChar(oneTeg.getCountChar());
        siteTagAnalyses.setCountInputPhrase(oneTeg.getCountDirectOccurrencesPhrase());
        siteTagAnalyses.setCountWord(oneTeg.getCountWord());
        siteTagAnalyses.setPositionFirstPhrasesWord(oneTeg.getFirstPositionOccurrences());

        return siteTagAnalyses;
    }

}
