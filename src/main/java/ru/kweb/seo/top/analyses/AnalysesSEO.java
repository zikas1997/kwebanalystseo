package ru.kweb.seo.top.analyses;

import ru.kweb.seo.top.model.SiteModel;

import java.util.Set;

/**
 * Класс являеться прослойкой для анализом всех тэгов и анализом всех сайтов

 * @version 1.0
 */
public interface AnalysesSEO {

    /** Метод проводит анализ над сайтом*/
    SiteModel getAnalysesSiteModel(SiteModel siteModel, String searchPhrases);

    /** Метод проводит анализ над списком сайтов*/
    Set<SiteModel> getAnalysesSetSiteModel(Set<SiteModel> siteModelsSet, String searchPhrases);
}
