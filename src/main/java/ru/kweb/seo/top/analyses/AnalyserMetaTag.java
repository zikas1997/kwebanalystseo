package ru.kweb.seo.top.analyses;

import ru.kweb.seo.top.model.ListSite;
import ru.kweb.seo.top.model.analyses.SiteMetaTagAnalyses;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.model.analyses.KeywordAnalyses;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс делает полный анализ по топу метатэов

 * @version 1.0
 */
public class AnalyserMetaTag {


    private List<String> keywords = new ArrayList<>();

    /** Возращаемая модель */
    private SiteMetaTagAnalyses siteMetaTagAnalyses;

    public AnalyserMetaTag() {
    }

    /**
     * Функция возвращаеть проанализированную модель по keyword и description
     * вызывает функцию {@link #getAllKeywords(ListSite)}
     * вызывает функцию {@link #getMapKeywordWithCountCoincidence(List)}
     * вызывает функцию {@link #sortedListTag(Map, int)}
     * @param listSite глобальная модель
     * @return возвращает монель с анализом
     * */
    public SiteMetaTagAnalyses createSiteMetaTagAnalyses(ListSite listSite) {

        siteMetaTagAnalyses = new SiteMetaTagAnalyses();


        List<String> listKeywords = getAllKeywords(listSite);


        Map<String, Integer> countCoincidence = getMapKeywordWithCountCoincidence(listKeywords);

        //Выдает топ "X" встречающихся кейвордов
        Set<KeywordAnalyses> metaTags = sortedListTag(countCoincidence,5);

        siteMetaTagAnalyses.setKeywordAnalyses(metaTags);

        return siteMetaTagAnalyses;
    }

    /**
     * Функция возращает все keywords списка траниц
     * вызывает функцию {@link #getKeywordSiteModel(SiteModel)}
     * @param listSite глобальная модель
     * @return возвращает список всех ключивых фраз со всех топ страниц по запросуц
     * */
    private List<String> getAllKeywords(ListSite listSite) {

        for (SiteModel siteModel : listSite.getSite()) {
            if (siteModel.getMetaTagKeywords().isKeywords())
                keywords.addAll(getKeywordSiteModel(siteModel));
        }
        return keywords;
    }

    /**
     * Функция возращает все keywords по 1 странциу
     * @param siteModel модель страницы
     * @return возвращает список всех ключивых фраз с 1 странциы
     * */
    private List<String> getKeywordSiteModel(SiteModel siteModel) {
        String[] subStr = siteModel.getMetaTagKeywords().getTextMetaTagKeywords().split(", ");
        //Выбираем ключивые слова не длинее 254 символов
        return Arrays.asList(subStr).stream()
                .filter((a)->a.length()<254)
                .collect(Collectors.toList());
    }

    /**
     * Функция определяет сколько keywords повторяються
     * @param keywords список всех слов
     * @return возвращает мапу где ключ это ключевая фраза, а значение колличество повторений
     * */
    private Map<String, Integer> getMapKeywordWithCountCoincidence(List<String> keywords) {

        Map<String, Integer> map = new HashMap<>();
        for (String str : keywords) {
            if (!map.containsKey(str)) {
                map.put(str, 1);
            } else {
                map.put(str, map.get(str) + 1);
            }
        }
        return map;
    }

    /**
     * Функция возвращает keywords
     * @param map map всех слов
     * @param countLimitPhrases значенеи задеться в ручную в поле класс, показывает сколько keywords отбереться в отчет
     * @return возвращает множество keywords
     * */
    private Set<KeywordAnalyses> sortedListTag(Map<String, Integer> map, int countLimitPhrases) {

        List<KeywordAnalyses> metaTags = new ArrayList<>();

        for (Map.Entry entry : map.entrySet()) {
            KeywordAnalyses keywordAnalyses = new KeywordAnalyses((String) entry.getKey(), (int) entry.getValue());
            metaTags.add(keywordAnalyses);
        }

        Set<KeywordAnalyses> sortedListTag = metaTags.stream()
                .sorted(((o1, o2) -> o2.getCount() - o1.getCount()))
                .limit(countLimitPhrases)
                .collect(Collectors.toSet());

        return sortedListTag;
    }

}
