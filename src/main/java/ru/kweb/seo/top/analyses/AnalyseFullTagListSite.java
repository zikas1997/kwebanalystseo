package ru.kweb.seo.top.analyses;

import ru.kweb.seo.top.model.ListSite;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.model.SiteTagAnalyses;
import ru.kweb.seo.top.model.report.ModelAnalyserTop;
import ru.kweb.seo.top.model.report.TagModel;

import java.util.ArrayList;
import java.util.List;
/**
 * Класс подгатавливает анализ по топу/сайту для отображение на Front-end

 * @version 1.0
 */
public class AnalyseFullTagListSite {

    /** Основная модель с анализом */
    private ModelAnalyserTop modelAnalyserTop;

    /** Полный анализ схожих тэгов-title- */
    private TagModel tagTitle = new TagModel();

    /** Полный анализ схожих тэгов -H1- */
    private TagModel tagH1 = new TagModel();

    /** Полный анализ схожих тэгов -H2- */
    private TagModel tagH2 = new TagModel();

    /** Полный анализ схожих тэгов -H3- */
    private TagModel tagH3 = new TagModel();

    /** Полный анализ схожих тэгов -H4- */
    private TagModel tagH4 = new TagModel();

    /** Полный анализ схожих тэгов -P- */
    private TagModel tagP = new TagModel();

    /** Колличество топ сайтов от 0 до 10 */
    private int countSite = 1;

    public AnalyseFullTagListSite() {
    }

    /**
     *  Процедура делает анализ для отчета по топу
     *  @param listSite сырые данные топ 10
     * */
    public void analyse(ListSite listSite) {
        countSite = listSite.getSite().size();
        for (SiteModel sm : listSite.getSite()) {
            //пробегаем все теги и собираем общуюю информацию
            doAnalyseTag(sm);
        }
        renameTag();
        analyseFullStatistic();
        createModelAnalyserTop(listSite.getSearchLine(), listSite.getRegionNumber());
    }

    /**
     *  Процедура делает анализ для отчета по сайту
     *  @param siteModel сырые данные по сайту
     *  @param searchPhrases поисковая фраза
     *  @param searchRegion поисковой регион
     * */
    public void analyse(SiteModel siteModel, String searchPhrases, String searchRegion) {
        doAnalyseTag(siteModel);
        renameTag();
        analyseFullStatistic();
        createModelAnalyserTop(searchPhrases, Integer.parseInt(searchRegion));
    }

    /**
     *  Процедура первичного анализа, определяет основные данные по анализу с учетов приорететов сайта
     *  Вызов функции {@link #analyseEveryTag(TagModel, SiteTagAnalyses, double)}
     *  @param siteModel сырые данные по сайту
     * */
    private void doAnalyseTag(SiteModel siteModel) {

        for (SiteTagAnalyses tagAnalyses : siteModel.getSiteTagAnalyses()) {
            double k = 1;
            //Данный блок распределяет коэфиценты по позициям
            switch (siteModel.getPosition()) {
                case 1:
                    k = 1;
                    break;
                case 2:
                    k = 1;
                    break;
                case 3:
                    k = 1;
                    break;
                case 4:
                    k = 0.95;
                    break;
                case 5:
                    k = 0.9;
                    break;
                case 6:
                    k = 0.8;
                    break;
                case 7:
                    k = 0.8;
                    break;
                case 8:
                    k = 0.8;
                    break;
                case 9:
                    k = 0.7;
                    break;
                case 10:
                    k = 0.7;
                    break;
            }
            if (tagAnalyses.getTegName().equals("p")) {
                tagP = analyseEveryTag(tagP, tagAnalyses, k);
            }
            if (tagAnalyses.getTegName().equals("h1")) {
                tagH1 = analyseEveryTag(tagH1, tagAnalyses, k);
            }
            if (tagAnalyses.getTegName().equals("h2")) {
                tagH2 = analyseEveryTag(tagH2, tagAnalyses, k);
            }
            if (tagAnalyses.getTegName().equals("h3")) {
                tagH3 = analyseEveryTag(tagH3, tagAnalyses, k);
            }
            if (tagAnalyses.getTegName().equals("h4")) {
                tagH4 = analyseEveryTag(tagH4, tagAnalyses, k);
            }
            if (tagAnalyses.getTegName().equals("title")) {
                tagTitle = analyseEveryTag(tagTitle, tagAnalyses, k);
            }
        }
    }

    /**
     *  Процедура на вход получает некий контейнер для анлиза {@link #tagH1}...{@link #tagP} и проанализирванный
     *  тэг {@link SiteTagAnalyses} после алгоритм добавляет этот анализ в контейнер
     *  @param tagModel один из тэгов для анализа
     *  @param tagAnalyses один из тэга сырых данных
     *  @param k коэффицент расчитываеться по позиции сайта
     * */
    private TagModel analyseEveryTag(TagModel tagModel, SiteTagAnalyses tagAnalyses, double k) {

        if (tagAnalyses.getCountChar() != 0)
            tagModel.setCount(tagModel.getCount() + 1);
        tagModel.setCountChar(tagModel.getCountChar() + tagAnalyses.getCountChar() * k);
        tagModel.setCountWord(tagModel.getCountWord() + tagAnalyses.getCountWord() * k);
        tagModel.setCountInputPhrases(tagModel.getCountInputPhrases() + (int) tagAnalyses.getCountInputPhrase() * k);
        tagModel.setFirstEntrySearchPhrases(tagModel.getFirstEntrySearchPhrases() + tagAnalyses.getPositionFirstPhrasesWord());

        double allWord = tagAnalyses.getCountWord();
        double sWord = tagAnalyses.getCountInputPhrase();
        double percent = sWord / allWord * 100;

        //Поиск минимального и максимального значения
        if ((tagModel.getMinChar() > tagAnalyses.getCountChar()) && tagAnalyses.getCountChar() != 0) {
            tagModel.setMinChar(tagAnalyses.getCountChar());
        }
        if ((tagModel.getMinWord() > tagAnalyses.getCountWord()) && tagAnalyses.getCountWord() != 0) {
            tagModel.setMinWord(tagAnalyses.getCountWord());
        }
        if ((tagModel.getMinPercentSearchPhrases() > percent) && percent != 0) {
            tagModel.setMinPercentSearchPhrases(percent);
        }
        if (tagModel.getMaxChar() < tagAnalyses.getCountChar()) {
            tagModel.setMaxChar(tagAnalyses.getCountChar());
        }
        if (tagModel.getMaxWord() < tagAnalyses.getCountWord()) {
            tagModel.setMaxWord(tagAnalyses.getCountWord());
        }
        if (tagModel.getMaxPercentSearchPhrases() < percent) {
            tagModel.setMaxPercentSearchPhrases(percent);
        }
        return tagModel;
    }

    /**
     * Процедура заполянет поля {@link TagModel#nameTag}
     * */
    private void renameTag() {
        tagP.setNameTag("p");
        tagH1.setNameTag("h1");
        tagH2.setNameTag("h2");
        tagH3.setNameTag("h3");
        tagH4.setNameTag("h4");
        tagTitle.setNameTag("title");
    }

    /**
     *  Процедура вторичного анализа для всех тэгов
     *  Вызов функции {@link #analyseStatisticsTag(TagModel)}
     * */
    private void analyseFullStatistic() {
        tagP = analyseStatisticsTag(tagP);
        tagH1 = analyseStatisticsTag(tagH1);
        tagH2 = analyseStatisticsTag(tagH2);
        tagH3 = analyseStatisticsTag(tagH3);
        tagH4 = analyseStatisticsTag(tagH4);
        tagTitle = analyseStatisticsTag(tagTitle);
    }

    /**
     *  Процедура вторичного анализа для всех тэгов определяет, идеальное значение для тэгов.
     *  Вызов функции {@link #rangeValues(TagModel)}
     *  @param tagModel хрнаилище с данными
     * */
    private TagModel analyseStatisticsTag(TagModel tagModel) {
        double countInputPhrases;
        double countWord;
        double countFirstEntrySearchPhrases;
        try {
            countInputPhrases = tagModel.getCountInputPhrases();
            countWord = tagModel.getCountWord();
            countFirstEntrySearchPhrases = tagModel.getFirstEntrySearchPhrases();
            tagModel.setPercentSearchPhrases(Math.round(countInputPhrases / countWord * 100));
            tagModel.setFirstEntrySearchPhrases(Math.round(countFirstEntrySearchPhrases / tagModel.getCount()));
        } catch (ArithmeticException e) {
            tagModel.setPercentSearchPhrases(-1);
            tagModel.setFirstEntrySearchPhrases(-1);
        }

        if(tagModel.getNameTag().equals("title")||tagModel.getNameTag().equals("h1")){
            try {
                tagModel.setCountChar((tagModel.getCountChar() / tagModel.getCount()));
                tagModel.setCountWord((tagModel.getCountWord() / tagModel.getCount()));
            } catch (ArithmeticException e) {
                tagModel.setCountChar(-1);
                tagModel.setCountWord(-1);
            }
        }else {
            try {
                tagModel.setCountChar((tagModel.getCountChar() / countSite));
                tagModel.setCountWord((tagModel.getCountWord() / countSite));
            } catch (ArithmeticException e) {
                tagModel.setCountChar(-1);
                tagModel.setCountWord(-1);
            }

        }
        tagModel = rangeValues(tagModel);
        return tagModel;
    }

    /**
     *  Процедура определяет коридор для иделаьного занчения
     *  @param tagModel хрнаилище с данными
     * */
    private TagModel rangeValues(TagModel tagModel){
        try {
            //Коридоры
            double k;
            double deltaChar = (tagModel.getMaxChar() - tagModel.getMinChar()) / 8;
            double deltaWord = (tagModel.getMaxWord() - tagModel.getMinWord()) / 8;
            double deltaPercentSP = 1;
            if(tagModel.getPercentSearchPhrases()==0){
                deltaPercentSP = 0;
            }
            if(tagModel.getPercentSearchPhrases()>0&&tagModel.getPercentSearchPhrases()<5){
                deltaPercentSP = 1;
            }
            if(tagModel.getPercentSearchPhrases()>5&&tagModel.getPercentSearchPhrases()<10){
                deltaPercentSP = tagModel.getPercentSearchPhrases()*0.4;
            }
            if(tagModel.getPercentSearchPhrases()>10&&tagModel.getPercentSearchPhrases()<15){
                deltaPercentSP = tagModel.getPercentSearchPhrases()*0.25;
            }
            if(tagModel.getPercentSearchPhrases()>15&&tagModel.getPercentSearchPhrases()<40){
                deltaPercentSP = tagModel.getPercentSearchPhrases()*0.2;
            }
            if(tagModel.getPercentSearchPhrases()>40&&tagModel.getPercentSearchPhrases()<100){
                deltaPercentSP = tagModel.getPercentSearchPhrases()*0.125;
            }

            //мин <Символы>
            k = tagModel.getCountChar() - deltaChar;
            tagModel.setMinChar(k);
            //мин <Слова>
            k = tagModel.getCountWord() - deltaWord;
            tagModel.setMinWord(k);
            //мин <Проценты>
            k = tagModel.getPercentSearchPhrases() - deltaPercentSP;
            tagModel.setMinPercentSearchPhrases(k);
            //макс <Символы>
            k = tagModel.getCountChar() + deltaChar;
            tagModel.setMaxChar(k);
            //макс <Слова>
            k = tagModel.getCountWord() + deltaWord;
            tagModel.setMaxWord(k);
            //макс <Проценты>
            k = tagModel.getPercentSearchPhrases() + deltaPercentSP;
            tagModel.setMaxPercentSearchPhrases(k);
        } catch (ArithmeticException e) {

        }
        return tagModel;
    }

    /**
     *  Процедура создает {@link #modelAnalyserTop} из хранилищ анализов по тэгам
     *  @param searchPhrases поисковая фраза
     *  @param searchRegion поисковой регион
     * */
    private void createModelAnalyserTop(String searchPhrases, int searchRegion){

        List<TagModel> tagModels = new ArrayList<>();
        tagModels.add(tagTitle);
        tagModels.add(tagH1);
        tagModels.add(tagH2);
        tagModels.add(tagH3);
        tagModels.add(tagH4);
        tagModels.add(tagP);

        modelAnalyserTop = new ModelAnalyserTop();
        modelAnalyserTop.setPhrases(searchPhrases);
        modelAnalyserTop.setRegion(searchRegion);
        modelAnalyserTop.setTags(tagModels);
    }

    /**
     *  Функция возвращает {@link #modelAnalyserTop} !!!Вызывать после анаиза!!!
     * */
    public ModelAnalyserTop getModelAnalyserTop() {
        return modelAnalyserTop;
    }
}
