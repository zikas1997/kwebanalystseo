package ru.kweb.seo.top.analyses;

import ru.kweb.seo.top.model.SiteModel;

import java.util.HashSet;
import java.util.Set;
/**
 * Класс иплементируеться от {@link AnalysesSEO}

 * @version 1.0
 */
public class AnalysesAllTagsSiteModelService implements AnalysesSEO{

    private AnalyserAllTagsSiteModel analysesModelSite = new AnalyserAllTagsSiteModel();

    public AnalysesAllTagsSiteModelService() {
    }


    @Override
    public SiteModel getAnalysesSiteModel(SiteModel siteModel, String searchPhrases) {
        return analysesModelSite.createTagAnalyses(siteModel,searchPhrases);
    }

    @Override
    public Set<SiteModel> getAnalysesSetSiteModel(Set<SiteModel> siteModelsSet,String searchPhrases) {
        Set<SiteModel> siteModelSet = new HashSet<>();
        for (SiteModel aSiteModelsSet : siteModelsSet) {
            siteModelSet.add(analysesModelSite.createTagAnalyses(aSiteModelsSet, searchPhrases));
        }
        return siteModelSet;
    }
}
