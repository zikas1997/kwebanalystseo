package ru.kweb.seo.top.analyses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kweb.seo.top.service.StemAnalyzer;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;

import java.util.*;

/**
 * Класс делает полный анализ по 1 тэгу. использует библиатеку MyStem класс {@link StemAnalyzer}

 * @version 1.0
 */
public class AnalyserOneTag {

    /** Logger */
    private final static Logger log = LoggerFactory.getLogger(AnalyserOneTag.class.getName());

    /** Колличество слов в тэге */
    private int countWord = 0;

    /** Колличество символов в тэге*/
    private int countChar = 0;

    /** Колличество ключивых слов в тэге*/
    private int countInputPhrase = 0;

    /** Позиция первого вхождения ключевого слова */
    private int positionFirstPhraseWord = 0;

    private List<String> searchWords;
    private List<String> tags;
    private String charLength;


    /**
     * Конструктор класса, для AnalyseOneTag проверяет валидность тэга (на null)
     * @param tagText текст тэга
     * @param searchPhrase поисковая фраза
     * */
    public AnalyserOneTag(String tagText, String searchPhrase) {
        charLength = tagText.replace(" ","");
        try {
            searchWords = StemAnalyzer.GetLex(searchPhrase,true);
        } catch (MyStemApplicationException | NullPointerException e) {
            log.error("My Stem exception",searchPhrase);
        }
        try {
            tags = StemAnalyzer.GetLex(tagText,true);
        } catch (MyStemApplicationException | NullPointerException  e) {
            log.error("My Stem exception",searchPhrase);
        }
        if(searchWords == null){
            searchWords = new ArrayList<>();
        }
        if(tags == null){
            tags = new ArrayList<>();
        }
    }

    /**
     * Функция возвращает колличество символов в тэге без пробелов
     * @return коллчиество симолов
     * */
    public int getCountChar() {
        return charLength.length();
    }

    /**
     * Функция возвращает колличество слов в тэге
     * @return коллчиество слов
     * */
    public int getCountWord() {
        countWord = tags.size();
        return countWord;
    }

    /**
     * Функция возвращает колличество слов совпадающих с ключивыми
     * @return коллчиество слов
     * */
    public double getCountDirectOccurrencesPhrase/*Колличесвто прямых вхождений*/() {

        countInputPhrase = 0;
        countWord = tags.size();

        for (String searchWord : searchWords) {
            for (String tag : tags) {
                if (searchWord.equals(tag))
                    countInputPhrase++;
            }
        }

        return (double) countInputPhrase;
    }

    /**
     * Функция возвращает первое вхождение ключевого слова
     * @return позиция первого слова
     * */
    public int getFirstPositionOccurrences/*Позиция слова, с первым совом поиска*/() {

        positionFirstPhraseWord = tags.size();

        for (int i = tags.size() - 1; i > -1; i--) {
            for (String searchWord : searchWords) {
                if (tags.get(i).equalsIgnoreCase(searchWord) && positionFirstPhraseWord > i) {
                    positionFirstPhraseWord = i;
                }
            }
        }

        if (positionFirstPhraseWord == tags.size()) {
            positionFirstPhraseWord = -1;
        }

        return ++positionFirstPhraseWord;//Выводим позицию, где 1 слово это 1 позиция
    }
}
