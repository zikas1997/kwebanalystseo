package ru.kweb.seo.updateBD.shedulerBD;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.kweb.seo.top.model.ListSite;
import ru.kweb.seo.top.repository.ListSiteRepository;

import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 * Класс по таймеру очищяет в базе устаревшие данные по анализу

 * @version 1.0
 */
@Component
public class SchedulerDropOldAnalysesBD {

    @Autowired
    ListSiteRepository listSiteRepositoryImplement;

    @Scheduled(fixedRate = 1000*60*60*24*14)
    public void dropOldDate(){
        Date nowDate = new Date();
        Stream<ListSite> listSiteStream = listSiteRepositoryImplement.findAll().stream();
        listSiteRepositoryImplement.delete(listSiteStream.filter((o1)->nowDate.getTime() - o1.getDate() > 1000*120).collect(Collectors.toList()));
    }
}
