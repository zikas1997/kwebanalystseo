package ru.kweb.seo.updateBD.shedulerBD;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.kweb.seo.top.model.position.SitePosition;
import ru.kweb.seo.top.repository.PositionSiteRepository;

import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Класс по таймеру очищяет в базе устаревшие данные по позициям

 * @version 1.0
 */
@Component
public class SchedulerDropOldPositionBD {
    @Autowired
    private PositionSiteRepository positionSiteRepository;

    @Scheduled(fixedRate = 1000*60*60*24*14)
    public void dropOldDate(){
        Date nowDate = new Date();
        Stream<SitePosition> listSiteStream = positionSiteRepository.findAll().stream();
        positionSiteRepository.delete(listSiteStream.filter((o1)->nowDate.getTime() - o1.getLastTime() > 1000*120).collect(Collectors.toList()));
    }
}
