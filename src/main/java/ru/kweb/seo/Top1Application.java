package ru.kweb.seo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Top1Application {

	/*---Logger---*/
	private final static Logger log = LoggerFactory.getLogger(Top1Application.class.getName());

	public static void main(String[] args) {
		log.debug("Starting Top1Application");
		SpringApplication.run(Top1Application.class, args);
		log.debug("Started Top1Application");
	}
}
