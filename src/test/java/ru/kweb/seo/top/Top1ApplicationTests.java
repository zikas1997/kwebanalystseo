package ru.kweb.seo.top;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kweb.seo.top.analyses.AnalysesAllTagsSiteModelService;
import ru.kweb.seo.top.model.report.ModelAnalyserTop;
import ru.kweb.seo.top.analyses.AnalyserMetaTag;
import ru.kweb.seo.top.analyses.AnalysesSEO;
import ru.kweb.seo.top.creator.CreatorDomainPages;
import ru.kweb.seo.top.creator.CreatorPositionTop10Site;
import ru.kweb.seo.top.creator.CreatorReportAnalysesService;
import ru.kweb.seo.top.ethernet.GetterHtmlPageByUrl;
import ru.kweb.seo.top.ethernet.HtmlUnitClient;
import ru.kweb.seo.top.ethernet.YandexRequestXmlProxy;
import ru.kweb.seo.top.json.JsonConvector;
import ru.kweb.seo.top.model.json.JsonRequest;
import ru.kweb.seo.top.model.ListSite;
import ru.kweb.seo.top.model.analyses.SiteMetaTagAnalyses;
import ru.kweb.seo.top.model.SiteModel;
import ru.kweb.seo.top.parsers.CreateSiteModelByHtmlPage;
import ru.kweb.seo.top.parsers.ParserSite;
import ru.kweb.seo.top.parsers.ParserResponse;
import ru.kweb.seo.top.parsers.ParserXmlResponse;
import ru.kweb.seo.top.repository.ListSiteRepository;
import ru.kweb.seo.top.repository.SiteModelRepository;
import ru.kweb.seo.top.service.StemAnalyzer;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class Top1ApplicationTests {

    /*---Logger---*/
    private final static Logger log = LoggerFactory.getLogger(Top1ApplicationTests.class.getName());

    private long st;
    private long en;
    private double rs;

    /*
     * Тест_1 "Проверка работоспособности модуля анализ"
     *
     * Тест проверяет работу модуля анализа
     *
     * Вход: Считывает файл с ответом yandex
     * Выход: List<SiteModel> - Список анализов 10 сайтов
     * */
    @Test
    public void testAnalyses() {
        createTop10("Разработка сайтов", 35);
    }


    /*Тест_2 "Проверка репозитоирия"
     *
     * Вход: Считывает файл с ответом yandex
     * TODO
     * */
    @Autowired
    SiteModelRepository siteModelRepository;
    @Autowired
    ListSiteRepository listSiteRepository;

    @Test
    public void testRepository() {
        List<SiteModel> smListAnalyses = createTop10("Разработка сайтов", 35);
        ListSite listSite = new ListSite();
        for (SiteModel sm : smListAnalyses) {
            sm.setListSite(listSite);
        }
        listSite.setRegionNumber(35);
        listSite.setSearchLine("Разработка сайтов");
        listSite.setSite(new HashSet<>(smListAnalyses));
        listSiteRepository.save(listSite);
        List<ListSite> siteModels = listSiteRepository.findAll();
        JsonConvector jsonConvector = new JsonConvector();
        String result = jsonConvector.serializeJsonListSite(siteModels.get(0));
        log.debug("Result test_2: " + result);
    }

    /*Тест_3 "Првоерка модуля работы с Json"
     *
     * Вход-выход тест_2
     * Выход: serializeJsonListSite
     * */
    @Test
    public void testJsonConvector() {
        JsonConvector jsonConvector = new JsonConvector();
        List<SiteModel> smListAnalyses = createTop10("Разработка сайтов", 35);
        ListSite listSite = new ListSite();
        for (SiteModel sm1 : smListAnalyses) {
            sm1.setListSite(listSite);
        }
        listSite.setRegionNumber(35);
        listSite.setSearchLine("аптеки");
        listSite.setSite(new HashSet<>(smListAnalyses));
        String str = jsonConvector.serializeJsonListSite(listSite);
        log.debug("Result test_3: " + str);
    }


    /*Тест_4 "Проверка полного анализа"
     *
     * Вход: Ответ xml Api (файл)
     * Выход: serializeJsonListSite
     * */
    //@Test
    public void testFullAnalysesXmlApi() {
        JsonConvector jsonConvector = new JsonConvector();
        List<SiteModel> smListAnalyses = createTop10("Перевозка грузов", 35);
        List<SiteModel> siteModels1 = siteModelListAnalyses(smListAnalyses, "Перевозка грузов");
        ListSite listSite = new ListSite();
        for (SiteModel sm1 : siteModels1) {
            sm1.setListSite(listSite);
        }
        listSite.setRegionNumber(54);
        listSite.setSearchLine("Перевозка грузов");
        listSite.setSite(new HashSet<>(siteModels1));
        listSite.setSiteMetaTagAnalyses(getSiteMetaTagAnalyses(listSite));
        String str = jsonConvector.serializeJsonListSite(listSite);
        log.debug("Result test_4: " + str);
    }

    /*Тест_5 "Проверка парсинга генерющихся сайтов"
     *
     * Вход: url сайт
     * Выход: html page
     * */
    @Test
    public void testConstuctorSite() {
        HtmlUnitClient htmlUnitClient = new HtmlUnitClient();
        log.debug("Result test_5: " + htmlUnitClient.getResponse("http://asg.vidigital.ru/1/778/i/h"));
    }

    /*Тест_6 "Анализ сайта пользвоателя"
     *
     * Вход: JsonRequest
     * Выход: SiteModel
     * */
    @Test
    public void testGettingListPagesByUrl() {

        CreatorDomainPages creatorDomainPages = new CreatorDomainPages();
        List<String> stringList = creatorDomainPages.getListPagesDomain(new JsonRequest("", "negabarite.com", 0));
        JsonConvector jsonConvector = new JsonConvector();
        String json = jsonConvector.serializeList(stringList);
        log.debug("Result test_6: " + json);
    }

    /*Тест7 "Проверка яндекс библиотеки mysteam"
     *
     * Вход: string
     * Выход: string[] начальная форма
     * */

    @Test
    public void testMySteam() throws MyStemApplicationException {

        List<String> lex = StemAnalyzer.GetLex("hellow привет как дела дождь красивый снег быстро красиво имя попы клоп hi man", true);

        for (String info : lex) {
            System.out.println(info);
        }
    }


    /*@Test
    public void test(){

         long start = System.nanoTime();
        //String request = "https://negabarite.ru";
        String request = "http://lco.expert";
      //  String request = "https://www.google.com/";
        WebClient webClient = new WebClient();
        webClient.getOptions().setJavaScriptEnabled(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setUseInsecureSSL(true);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setDownloadImages(false);
        webClient.getOptions().setGeolocationEnabled(false);
        webClient.getOptions().setRedirectEnabled(false);
        webClient.getOptions().setPrintContentOnFailingStatusCode(false);
        webClient.getOptions().setTimeout(5500);


        if(isCms(request+"/wp-admin",webClient)||isCms(request+"/wp-login.php",webClient)){
            System.out.println("WordPress");
        }
        if(isCms(request+"/user",webClient)){
            System.out.println("Drupal");
        }
        if(isCms(request+"/administrator",webClient)){
            System.out.println("Joomla");
        }
        if(isCms(request+"/admin",webClient)){
            System.out.println("Host CMS");
        }
        if(isCms(request+"/login",webClient)){
            System.out.println("WebAsyst || InstantCMS || Magneto");
        }
        if(isCms(request+"/manager",webClient)){
            System.out.println("MODX");
        }
        if(isCms(request+"/bitrix",webClient)){
            System.out.println("Bitrix");
        }
        en = System.nanoTime();
        rs = en - start;
        log.debug("All : " + rs / 1000000000);

    }*/

    @Autowired
    CreatorReportAnalysesService creatorReportAnalysesService;

    //@Test
    public void testExcel() {

        st = System.nanoTime();

        //creatorReportAnalyses.create(new String[]{"негаюаритные перевозки", "перевозка трактаров", "перевозка негабаритных грузов"}, 35);
       /* List<ModelAnalyserTop> tops = creatorReportAnalyses.getReportAnalyses();
        for (ModelAnalyserTop modelAnalyserTop : tops) {
            System.out.println(modelAnalyserTop.toString());
        }*/
        ModelAnalyserTop top = creatorReportAnalysesService.create("негабаритные перевозки", 35);
        en = System.nanoTime();
        rs = en - st;
        log.debug("End : " + rs / 1000000000);

    }
/*
    @Autowired
    EmailSender emailSender;
    @Test
    public void testMail() {
         emailSender.sendMessageWithAttachment("zikas1997@gmail.com","Hi, Evgeny","Hi, what?","D:\\java\\employee.xls");
    }

    @Test
    public void testMobileVersion() {
        HttpApacheClient httpApacheClient = new HttpApacheClient();
        try {
            httpApacheClient.existMobileVersion(
                    "https://searchconsole.googleapis.com/v1/urlTestingTools/mobileFriendlyTest:run?key=AIzaSyCp6uWNFPcMuuOU9ytXWM0iMYdxFtk_gkI",
                    "https://negabarite.com/");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("ошибка!");
        }
    }*/

    private boolean isCms(String request, WebClient webClient) {
        String result;
        st = System.nanoTime();
        Page page = null;
        WebResponse response = null;
        try {
            if (webClient.getPage(request).getWebResponse().getStatusCode() == 200 || webClient.getPage(request).getWebResponse().getStatusCode() == 301) {
                page = webClient.getPage(request);
                System.out.println("Время отклика: " + page.getWebResponse().getLoadTime());
                response = page.getWebResponse();
                result = response.getContentAsString();
                log.error("Result: " + result);
                en = System.nanoTime();
                rs = en - st;
                log.debug("End : " + rs / 1000000000);
                return true;
            } else {
                log.error("Status code: " + webClient.getPage(request).getWebResponse().getStatusCode());
            }
        } catch (SocketTimeoutException e) {
            log.error("Time out exception url: " + request);
        } catch (UnknownHostException e1) {
            log.error("URL exception: " + request, e1);
        } catch (IOException e) {
            log.error("URL exception: " + request, e);
        }
        webClient.close();
        en = System.nanoTime();
        rs = en - st;
        log.debug("End : " + rs / 1000000000);
        return false;
    }

    private SiteModel siteModelAnalyses(SiteModel sm, String searchPhrase) {
        SiteModel siteModel;
        AnalysesSEO seo = new AnalysesAllTagsSiteModelService();
        siteModel = seo.getAnalysesSiteModel(sm, searchPhrase);
        return siteModel;
    }

    private List<SiteModel> siteModelListAnalyses(List<SiteModel> smList, String searchPhrase) {
        AnalysesSEO seo = new AnalysesAllTagsSiteModelService();
        Set<SiteModel> siteModelSet = new HashSet<>(smList);
        return new ArrayList<>(seo.getAnalysesSetSiteModel(siteModelSet, searchPhrase));
    }

    private List<SiteModel> createTop10(String searchPhrases, int regionNumber) {

        List<SiteModel> top10smList = new ArrayList<>();

        //-------------------------------------------------------------------------------------------
        //  Получаем топ 10 URL из ответа Yandex.
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        String responseXml = YandexRequestXmlProxy.getResponse(new JsonRequest(searchPhrases, regionNumber));
        en = System.nanoTime();
        rs = en - st;
        log.debug("Time get http://xmlproxy.ru response: " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------
        //Получаем топ 10 URL из ответа Yandex.
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        ParserResponse topSite = new ParserXmlResponse();
        List<String> top10SiteUrl = topSite.getUrlTopSiteWithFilter(responseXml);
        for (String str : top10SiteUrl) {
            System.out.println(str);
        }
        en = System.nanoTime();
        rs = en - st;
        log.debug("Time get top 10 URL : " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------


        //-------------------------------------------------------------------------------------------
        //Получаем в много-поточном режиме все htmlPage
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        List<String> htmlPageList = GetterHtmlPageByUrl.getTopHtml(top10SiteUrl);
        en = System.nanoTime();
        rs = en - st;
        log.debug("Time get top 10 html page : " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------
        // Создаем топ 10 SiteModel на основе html page
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        ParserSite createSiteModelByHtmlPage = new CreateSiteModelByHtmlPage();
        int i = 0;
        for (String htmlPage : htmlPageList) {
            SiteModel sm = createSiteModelByHtmlPage.getSiteModel(htmlPage, top10SiteUrl.get(i));
            if (sm != null)
                top10smList.add(sm);
            i++;
        }
        en = System.nanoTime();
        rs = en - st;
        log.debug("Time get top 10 SiteModel by htm pages: " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------


        //-------------------------------------------------------------------------------------------
        //Дописываем позиции сайтам
        //-------------------------------------------------------------------------------------------
        st = System.nanoTime();
        top10smList = CreatorPositionTop10Site.creatorPosition(top10smList, top10SiteUrl);
        en = System.nanoTime();
        rs = en - st;
        log.debug("Time get top 10 SiteModel + position : " + rs / 1000000000);
        //-------------------------------------------------------------------------------------------

        return top10smList;
    }

    private SiteMetaTagAnalyses getSiteMetaTagAnalyses(ListSite listSite) {
        AnalyserMetaTag analyserMetaTag = new AnalyserMetaTag();
        return analyserMetaTag.createSiteMetaTagAnalyses(listSite);
    }


}
